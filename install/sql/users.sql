PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE users (salutation TEXT, job_title TEXT, first_name TEXT, last_name TEXT, email TEXT UNIQUE NOT NULL, user_name TEXT UNIQUE, password TEXT, affiliation TEXT, country TEXT, city TEXT, comment TEXT, date_registered TEXT, disabled INTEGER DEFAULT 0, assoc INTEGER, ip TEXT, activation_code TEXT, disabled_reason TEXT, date_requested TEXT, wide_screen INTEGER DEFAULT 1);
INSERT INTO "users" VALUES('','','','','demo@demo.demo','demo',%s,'','','','',%s,0,%s,'','','','',1);
INSERT INTO users (email, user_name, password, assoc, date_registered) VALUES (###);
COMMIT;
