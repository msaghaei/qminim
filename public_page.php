<?php

/**
 * @file public_page.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */

if (!defined('GATE_PASSED')) exit();

$action = get_user_var('action', 'intro_page');
$arr = array('login_page', 'login_data', 'pre_register', 'pre_register_data', 'register', 'register_data', 'activation_code_data', 'login_problem', 'login_problem_data', 'password_reset');
if (! in_array($action, $arr)) {
    $action = "intro_page";
}

if (!strstr($action, 'login') && !strstr($action, 'register') && !strstr($action, 'intro_page') && !strstr($action, 'activation') && !strstr($action, 'password_reset'))
    $action = "login_page";

switch($action) {
    case "intro_page":
        $smarty->display('login.tpl');
        break;
	case "pre_register":
		$smarty->display('pre_register.tpl');
        break;
	case "register":
	    $smarty->assign('salutations', get_salutation());
	    $smarty->assign('job_titles', get_job_titles());
	    $smarty->assign('countries', get_countries());
	    $smarty->assign('eula', get_eula());
        if (Settings::getSetting('register_captcha'))
            $smarty->assign('show_captcha', true);
        else
            $smarty->assign('show_captcha', false);
		$smarty->display('register.tpl');
		break;
	case "pre_register_data":
	    manage_pre_register_data();
		break;
    case "activation_code_data":
	    manage_activation_code_data();
		break;
	case "register_data":
	    manage_register_data();
		break;
	case "login_page":
		$smarty->display('login.tpl');
		break;
	case "login_data":
		validate_user();
		break;
    case "login_problem":
		$smarty->display('login_problem.tpl');
		break;
    case "login_problem_data":
        manage_login_problem();
        break;
    case "password_reset":
        manage_reset_password();
        break;
}
?>
