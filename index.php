<?php

/**
 * @file index.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


session_start();
define('GATE_PASSED', true);
error_reporting(E_ALL);

$base_url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$base_url = str_replace('index.php', '', $base_url);
define('BASE_URL', $base_url); // set to the subfolder at the doc root

if (file_exists(dirname(__FILE__) . "/config.inc.php"))
    require_once(dirname(__FILE__) . "/config.inc.php");

$uc_file = 'OUT_OF_SERVICE';
if (file_exists($uc_file)) {
    $uc = file_get_contents($uc_file);
    if (($uc - time()) > 0) {
        $return_date = date('l jS \of F Y h:i:s A', $uc);
        $content = file_get_contents('OUT_OF_SERVICE_TEMPLATE');
        echo sprintf($content, BASE_URL, $return_date);
        exit();
    }
}

if (!defined('INSTALLED')) {
    $base_url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    $base_url = str_replace('index.php', '', $base_url);
    header("Location: " . $base_url . 'install/install.php');
    exit();
} else {
    if (file_exists(BASE_DIR . 'install'))
        die('Please delete the install folder and try again!');
}

require_once(dirname(__FILE__) . "/init.php");

check_csrf();

if (!isset($_SESSION['userloggedin'])) {
    require_once(BASE_DIR . 'public_page.php');
} else {
    require_once(BASE_DIR . 'private_page.php');
}

?>
