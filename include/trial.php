<?php

/**
 * @file trial.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */



if (!defined('GATE_PASSED')) exit(); 

require_once('minim.php');

class Trial {

    var $db;
    var $db_file;
    var $treatments;
    var $factors;
    var $prob_method;
    var $base_prob;
    var $dist_method;
    var $preload;
    var $subjects;
    var $frequencies;
    var $balance;
    var $user;
    var $status;
    var $testmode;
    var $restored = false;
    
    function Trial($user) {
        $this->testmode = false;
        $this->user = $user;
        $this->init_trial();
    }

    function init_trial() {
        $this->init_db();
        $this->init_treatments();
        $this->init_factors();
        $this->init_settings();
        $this->init_preload();
        $this->init_subjects();
        $this->update_frequencies();
        $this->set_status();
    }

    function set_status() {
        $this->status = 'Setup';
        if ($this->set_status_finished())
            return;
        if ($this->set_status_enroll())
            return;
        array_walk_recursive($this->preload, array($this, 'set_status_preload'));
    }

    function set_status_preload($load, $key) {
        if ($load > 0)
            $this->status = 'Preload';
    }

    function set_status_enroll() {
        if (count($this->subjects) > 0) {
            $this->status = 'Enroll';
            return true;
        }
        return false;
    }

    function set_status_finished() {
        if (count($this->subjects) == $this->sample_size) {
            $this->status = 'Finished';
            return true;
        }
        return false;
    }

    function get_levels($factor) {
        $levels = array();
        $query = "SELECT level FROM levels WHERE factor=%s;";
        $query = sprintf($query, $this->db->quote($factor));
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $levels[] = $row['level'];
            }
            return $levels;
        } else {
            $this->restore_trial_or_die($error);
        }
    }
    
    function init_subject_levels(&$subject) {
        $subject_id = $subject['id'];
        $query = "SELECT factor, level FROM subject_levels WHERE subject_id=%s;";
        $query = sprintf($query, $this->db->quote($subject_id));
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                if ($row['factor'] == 'treatment') continue;
                $subject[$row['factor']] = $row['level'];
            }
        } else {
            $this->restore_trial_or_die($error);
        }
    }

    function update_frequencies() {
        $this->frequencies = $this->preload;
        foreach ($this->subjects as $id => $subject) {
            $t = $subject['treatment'];
            foreach ($this->factors as $f => $v) {
                $l = $subject[$f];
                $this->frequencies[$t][$f][$l]++;
            }
        }
        $this->balance = $this->get_trial_balance();
    }

    function get_minim_model() {
        $minim = new Minim($this);
        $minimum_treatment = array_search(min($this->treatments), $this->treatments);
        $minim->minimum_treatment = $minimum_treatment;
        $minim->prob_method = $this->prob_method;
        $minim->dist_method = $this->dist_method;
        $minim->base_prob = $this->base_prob;
        $minim->build_probs();
        return $minim;
    }

    function get_used_ids() {
        if ($this->recycle_ids) {
            return array_keys($this->subjects);
        } else {
            $ret = array();
            $query = "SELECT * FROM used_ids;";
            if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
                while($row = $result->fetch()) {
                    $ret[] = $row['id'];
                }
                return $ret;
            } else {
                $this->restore_trial_or_die($error);
            }
        }
    }

    function calc_id($n) {
        switch ($this->id_type) {
            case "Numeric":
                $id = $this->pad_id_numeric($n);
                break;
            case "Alpha":
                $id = $this->convert_to_alpha($n);
                break;
            case "Alphanumeric":
                $id = $this->convert_to_alphanumeric($n);
                break;
        }
        return $id;
    }

    function get_extra_subject_id($used_ids) {
        // this is plan B, when we run out of original ID pools
        $n = $this->sample_size;
        $id = $this->calc_id($n);
        while (in_array($id, $used_ids)) {
            $n++;
            // gives only sequential id. need fix
            $id = $this->calc_id($n);
        }
        return $id;
    }

    function get_next_subject_id() {
        $ids = range(1, $this->sample_size);
        switch ($this->id_type) {
            case "Numeric":
                $ids = array_map(array($this, 'pad_id_numeric'), $ids);
                break;
            case "Alpha":
                $ids = array_map(array($this, 'convert_to_alpha'), $ids);
                break;
            case "Alphanumeric":
                $ids = array_map(array($this, 'convert_to_alphanumeric'), $ids);
                break;
        }
        if ($this->id_order == 'Random')
            shuffle($ids);
        $used_ids = $this->get_used_ids();
        $ids = array_diff($ids, $used_ids);
        if (count($ids) == 0) {
            return $this->get_extra_subject_id($used_ids);
        }
        reset($ids);
        return current($ids);
    }

    function get_subject($id) {
        $query = "SELECT * FROM subjects WHERE id=%s;";
        $query = sprintf($query, $this->db->quote($id));
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            $row = $result->fetch();
            $subject = array();
            $subject['id'] = $row['id'];
            $subject['treatment'] = $row['treatment'];
            $this->init_subject_levels($subject);    
            $subject['enrolled_by'] = $row['enrolled_by'];
            $subject['date_enrolled'] = $row['date_enrolled'];
            $subject['date_modified'] = $row['date_modified'];
        } else {
            $this->restore_trial_or_die($error);
        }
        return $subject;
    }

    function init_treatments() {
        $this->treatments = array();
        $query = "SELECT * FROM treatments;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $this->treatments[$row["treatment"]] = $row["ratio"];
            }
        } else {
            $this->restore_trial_or_die($error);
        }
    }
    
    function insert_treatment($treatment, $ratio) {
        $query = "INSERT INTO treatments (treatment, ratio) " . "VALUES (###);";
        $params = array($treatment, $ratio);
        $query = $this->db->replace_params($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $this->treatments[$treatment] = $ratio;
        $this->clear_preload();
        unset($_SESSION['subjectsfilter']);
    }

    function update_treatment($old_treatment, $new_treatment, $ratio) {
        $query = "UPDATE treatments SET treatment=%s, ratio=%s " .
          "WHERE treatment=%s;";
        $params = array($new_treatment, $ratio, $old_treatment);
        $params = array_map(array($this->db, 'quote'), $params);
        $query = vsprintf($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $query = "UPDATE preload SET treatment=%s WHERE treatment=%s;";
        $params = array($new_treatment, $old_treatment);
        $params = array_map(array($this->db, 'quote'), $params);
        $query = vsprintf($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $query = "UPDATE subjects SET treatment=%s WHERE treatment=%s;";    
        $params = array($new_treatment, $old_treatment);
        $params = array_map(array($this->db, 'quote'), $params);
        $query = vsprintf($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $this->init_treatments();
        $this->init_preload();
        $this->init_subjects();
        $this->update_frequencies();
        unset($_SESSION['subjectsfilter']);
    }

    function delete_treatment($treatment) {
        $query = "DELETE FROM treatments WHERE treatment=%s;";
        $query = sprintf($query, $this->db->quote($treatment));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        unset($this->treatments[$treatment]);
        $this->clear_preload();
        unset($_SESSION['subjectsfilter']);
    }

    function init_factors() {
        $this->factors = array();
        $query = "SELECT * FROM factors;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $this->factors[$row["factor"]] = array($row["weight"], $this->get_levels($row["factor"]));
            }
        } else {
            $this->restore_trial_or_die($error);
        }
    }

    function insert_factor($factor, $weight, $levels) {
        $query = "INSERT INTO factors (factor, weight) " .
          "VALUES (###);";
        $params = array($factor, $weight);
        $query = $this->db->replace_params($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        foreach($levels as $level) {
            $query = "INSERT INTO levels (factor, level) " .
              "VALUES (###);";
            $params = array($factor, $level);
            $query = $this->db->replace_params($query, $params);
            if(!$this->db->queryExec($query, $error))
            {
              $this->restore_trial_or_die($error);
            }
        }
        $this->factors[$factor] = array($weight, $levels);
        $this->clear_preload();
        unset($_SESSION['subjectsfilter']);
    }

    function update_factor($old_factor, $new_factor, $weight, $levels) {
        $query = "UPDATE factors SET factor=%s, weight=%s " .
          "WHERE factor=%s;";    
        $params = array($new_factor, $weight, $old_factor);
        $params = array_map(array($this->db, 'quote'), $params);
        $query = vsprintf($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $query = "DELETE FROM levels WHERE factor=%s";
        $query = sprintf($query, $this->db->quote($old_factor));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        foreach($levels as $level) {
            $query = "INSERT INTO levels (factor, level) " .
              "VALUES (###);";
            $params = array($new_factor, $level);
            $query = $this->db->replace_params($query, $params);
            if(!$this->db->queryExec($query, $error))
            {
              $this->restore_trial_or_die($error);
            }
        }
        $old_levels = $this->factors[$old_factor][1];
        if ($this->status != 'Setup') {
            $query = "DELETE FROM preload WHERE factor=%s";
            $query = sprintf($query, $this->db->quote($old_factor));
            if(!$this->db->queryExec($query, $error))
            {
              $this->restore_trial_or_die($error);
            }
            foreach ($this->treatments as $treatment => $r) {
                for ($i=0;$i<count($levels);$i++) {
                    $old_level = $old_levels[$i];
                    $new_level = $levels[$i];
                    $load = $this->preload[$treatment][$old_factor][$old_level];
                    $query = "INSERT INTO preload (treatment, factor, level, total) " .
                        "VALUES (###);";
                    $params = array($treatment, $new_factor, $new_level, $load);
                    $query = $this->db->replace_params($query, $params);
                    if(!$this->db->queryExec($query, $error))
                    {
                      $this->restore_trial_or_die($error);
                    }
                }
            }
            if (count($this->subjects)) {
                foreach ($this->subjects as $id => $subject) {
                    for ($i=0;$i<count($levels);$i++) {
                        $old_level = $old_levels[$i];
                        $new_level = $levels[$i];
                        if ($new_factor == 'treatment') continue;
                        $query = "UPDATE subject_levels SET factor=%s, level=%s " .
                        "WHERE subject_id=%s AND factor=%s AND level=%s;";
                        $params = array($new_factor, $new_level, $id, $old_factor, $old_level);
                        $params = array_map(array($this->db, 'quote'), $params);
                        $query = vsprintf($query, $params);
                        if(!$this->db->queryExec($query, $error))
                        {
                          $this->restore_trial_or_die($error);
                        }
                    }
                }
            }
            $this->init_factors();
            $this->init_preload();
            $this->init_subjects();
            $this->update_frequencies();
        } else {
            $this->init_factors();
            $this->clear_preload();
        }
        unset($_SESSION['subjectsfilter']);
    }

    function delete_factor($factor) {
        $query = "DELETE FROM factors WHERE factor=%s";
        $query = sprintf($query, $this->db->quote($factor));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $query = "DELETE FROM levels WHERE factor=%s";
        $query = sprintf($query, $this->db->quote($factor));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        unset($this->factors[$factor]);
        $this->clear_preload();
        unset($_SESSION['subjectsfilter']);
    }

    function init_settings() {
        $query = "SELECT * FROM settings;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            $row = $result->fetch();
            if (array_key_exists('title', $row)) {
                $this->title = $row['title'];
            } else {
                $sql = "ALTER TABLE settings ADD COLUMN title TEXT DEFAULT ''";
                if (!$this->db->queryExec($sql, $error))
                    die($error);
                #header("Location: " . BASE_URL . 'index.php');
                $this->title = '';
            }
            if (array_key_exists('recycle_ids', $row)) {
                $this->recycle_ids = $row['recycle_ids'];
            } else {
                $sql = "ALTER TABLE settings ADD COLUMN recycle_ids INTEGER DEFAULT 1";
                if (!$this->db->queryExec($sql, $error))
                    die($error);
                $this->recycle_ids = 0;
            }
            $this->prob_method = $row['prob_method'];
            $this->base_prob = $row['base_prob'];
            $this->dist_method = $row['dist_method'];
            $this->sample_size = $row['sample_size'];
            $this->id_type = $row['id_type'];
            $this->id_order = $row['id_order'];
        } else {
            $this->restore_trial_or_die($error);
        }
    }

    function update_settings($prob_method, $base_prob, $dist_method, $sample_size, $id_type, $id_order, $title='', $recycle_ids=0) {
        $query = "UPDATE settings SET prob_method=%s, base_prob=%s, dist_method=%s, sample_size=%s, id_type=%s, id_order=%s, title=%s, recycle_ids=%s;";
        $params = array($prob_method, $base_prob, $dist_method, $sample_size, $id_type, $id_order, $title, $recycle_ids);
        $params = array_map(array($this->db, 'quote'), $params);
        $query = vsprintf($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $this->title = $title;
        $this->recycle_ids = $recycle_ids;
        $this->prob_method = $prob_method;
        $this->base_prob = $base_prob;
        $this->dist_method = $dist_method;
        $this->sample_size = $sample_size;
        $this->id_type = $id_type;
        $this->id_order = $id_order;
        $this->set_status();
    }

    function update_preload($preload) {
        $query = "DELETE FROM preload;";
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        foreach($preload as $t => $factors) {
            foreach($factors as $f => $levels) {
                foreach($levels as $l => $total) {
                    $query = "INSERT INTO preload (treatment, factor, level, total) " .
                    "VALUES(###);";
                    $params = array($t, $f, $l, $total);
                    $query = $this->db->replace_params($query, $params);
                    if(!$this->db->queryExec($query, $error))
                    {
                      $this->restore_trial_or_die($error);
                    }
                }
            }
        }
        $this->preload = $preload;
        $this->update_frequencies();
        $this->set_status();
    }

    function get_preload_transpose() {
        $preloadtp = array();
        $t = array();
        foreach ($this->factors as $factor => $value) {
            $preloadtp[$factor] = array();
            $levels = $value[1];
            foreach ($levels as $level) {
                $preloadtp[$factor][$level] = array();
                foreach ($this->treatments as $treatment => $ratio) {
                    if (! array_key_exists($treatment, $t))
                        $t[$treatment] = 0;
                    $t[$treatment] += $this->preload[$treatment][$factor][$level];
                    $preloadtp[$factor][$level][$treatment] = $this->preload[$treatment][$factor][$level];
                }
            }
        }
        foreach($t as $treatment => $p) {
            $t[$treatment] = $p / count($this->factors);
        }
        $preloadtp['Total'] = $t;
        return $preloadtp;
    }

    function get_frequencies() {
        $ip = $_SESSION['subjectsfilter']['include_preload'];
        if ($ip)
            return $this->frequencies;
        $a = $this->frequencies;
        foreach ($this->treatments as $t => $r) {
            foreach ($this->factors as $f => $v) {
                foreach ($v[1] as $l) {
                    $a[$t][$f][$l] = $this->frequencies[$t][$f][$l] - $this->preload[$t][$f][$l];
                }
            }
        }
        return $a;
    }

    function get_frequencies_transpose() {
        $ep = !$_SESSION['subjectsfilter']['include_preload'];
        $frequenciestp = array();
        $t = array();
        foreach ($this->factors as $factor => $value) {
            $frequenciestp[$factor] = array();
            $levels = $value[1];
            foreach ($levels as $level) {
                $frequenciestp[$factor][$level] = array();
                foreach ($this->treatments as $treatment => $ratio) {
                    if (! array_key_exists($treatment, $t))
                        $t[$treatment] = 0;
                    $t[$treatment] += $this->frequencies[$treatment][$factor][$level];
                    if ($ep)
                        $t[$treatment] -= $this->preload[$treatment][$factor][$level];
                    $frequenciestp[$factor][$level][$treatment] = $this->frequencies[$treatment][$factor][$level];
                    if ($ep)
                        $frequenciestp[$factor][$level][$treatment] -= $this->preload[$treatment][$factor][$level];
                }
            }
        }
        foreach($t as $treatment => $p) {
            $t[$treatment] = $p / count($this->factors);
        }
        $frequenciestp['Total'] = $t;
        return $frequenciestp;
    }

    function clear_preload() {
        $query = "DELETE FROM preload;";
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $a = array();
        foreach ($this->treatments as $t => $r) {
            $a[$t] = array();
            foreach ($this->factors as $f => $v) {
                $a[$t][$f] = array();
                foreach ($v[1] as $l) {
                    $a[$t][$f][$l] = 0;
                    $query = "INSERT INTO preload (treatment, factor, level, total) " .
                             "VALUES (###);";    
                    $params = array($t, $f, $l, 0);
                    $query = $this->db->replace_params($query, $params);
                    if(!$this->db->queryExec($query, $error))
                    {
                      $this->restore_trial_or_die($error);
                    }
                }
            }
        }
        $this->preload = $a;
        $this->update_frequencies();
        $this->set_status();
    }

    function init_preload() {
        $a = array();
        foreach ($this->treatments as $t => $r) {
            $a[$t] = array();
            foreach ($this->factors as $f => $v) {
                $a[$t][$f] = array();
                foreach ($v[1] as $l) {
                    $a[$t][$f][$l] = 0;
                }
            }
        }
        $this->preload = $a;
        $query = "SELECT * FROM preload;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $t = $row['treatment'];
                $f = $row['factor'];
                $l = $row['level'];
                $this->preload[$t][$f][$l] = $row['total'];
            }
        } else {
            $this->restore_trial_or_die($error);
        }
        $this->set_status();
    }

    function check_create_used_ids_table() {
        $query = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='used_ids';";
        if($result = $this->db->query($query, SQLITE_NUM, $error)) {
            if ($row = $result->fetch()) {
                if ($row[0] > 0) return;
            }
        } else {
            die($error);
        }
        $sql = "CREATE TABLE used_ids (id TEXT PRIMARY KEY NOT NULL);";
        if(!$this->db->queryExec($sql, $error))
        {
          $this->restore_trial_or_die($error);
        }
        foreach ($this->subjects as $id => $subject) {
            $sql = "INSERT INTO used_ids (id) VALUES (###);";    
            $params = array($id);
            $sql = $this->db->replace_params($sql, $params);
            if(!$this->db->queryExec($sql, $error))
            {
              $this->restore_trial_or_die($error);
            }
        }
    }

    function init_subjects() {
        $this->subjects = array();
        $query = "SELECT * FROM subjects;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $subject = array();
                foreach($row as $k => $v) {
                    $subject[$k] = $v;
                }
                $this->init_subject_levels($subject);
                $this->subjects[$row['id']] = $subject;
            }
        } else {
            $this->restore_trial_or_die($error);
        }
        $this->check_create_used_ids_table();
    }

    function enroll_all() {
        $subjects =array();
        while ((count($this->subjects)+count($subjects)) < $this->sample_size) {
            $subject = array();
            foreach($this->factors as $factor => $v) {
                $levels = $v[1];
                $rk = array_rand($levels);
                $subject[$factor] = $levels[$rk];
            }
            $subjects[] = $subject;
        }

        $minim = $this->get_minim_model();
	    $d = date('Y-m-d H:i:s');
        $minim->enroll($subjects);
        foreach ($subjects as $subject) {
	        $id = $this->get_next_subject_id();
            $treatment = $subject['treatment'];
            $user_name = $this->user["user_name"];
            $query = "INSERT INTO subjects (id, treatment, enrolled_by, date_enrolled, date_modified) " .
                "VALUES (###);";    
            $params = array($id, $treatment, $user_name, $d, $d);
            $query = $this->db->replace_params($query, $params);
            if(!$this->db->queryExec($query, $error))
            {
              $this->restore_trial_or_die($error);
            }
            $query = "SELECT COUNT(*) FROM used_ids WHERE id=%s";
            $query = sprintf($query, $this->db->quote($id));
            if($result = $this->db->query($query, SQLITE_NUM, $error)) {
                if ($row = $result->fetch()) {
                    if ($row[0] > 0) continue;
                }
            } else {
                die($error);
            }
            $query = "INSERT INTO used_ids (id) VALUES (###);";    
            $params = array($id);
            $query = $this->db->replace_params($query, $params);
            if(!$this->db->queryExec($query, $error))
            {
              $this->restore_trial_or_die($error);
            }
            foreach($subject as $f => $l) {
                if ($f == 'treatment') continue;
                $query = "INSERT INTO subject_levels (subject_id, factor, level) " .
                "VALUES (###);";
                $params = array($id, $f, $l);
                $query = $this->db->replace_params($query, $params);
                if(!$this->db->queryExec($query, $error))
                {
                  $this->restore_trial_or_die($error);
                }
            }
            $subject['id'] = $id;
            $subject['enrolled_by'] = $user_name;
            $subject['date_enrolled'] = $d;
            $subject['date_modified'] = $d;
            $this->subjects[$id] = $subject;
        }
        $this->status = 'Finished';
    }

    function insert_subject($subject) {
	    $id = $this->get_next_subject_id();
	    $d = date('Y-m-d H:i:s');
        $minim = $this->get_minim_model();
        $subjects = array($subject);
        $minim->enroll($subjects);
        $subject = $subjects[0];
        $treatment = $subject['treatment'];
        $user_name = $this->user["user_name"];
        $query = "INSERT INTO subjects (id, treatment, enrolled_by, date_enrolled, date_modified) " .
          "VALUES (###);";    
        $params = array($id, $treatment, $user_name, $d, $d);
        $query = $this->db->replace_params($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $query = "SELECT COUNT(*) FROM used_ids WHERE id=%s";
        $query = sprintf($query, $this->db->quote($id));
        if($result = $this->db->query($query, SQLITE_NUM, $error)) {
            if ($row = $result->fetch()) {
                if ($row[0] == 0) {
                    $query = "INSERT INTO used_ids (id) VALUES (###);";    
                    $params = array($id);
                    $query = $this->db->replace_params($query, $params);
                    if(!$this->db->queryExec($query, $error))
                    {
                      $this->restore_trial_or_die($error);
                    }
                }
            }
        } else {
            die($error);
        }
        foreach($subject as $f => $l) {
            if ($f == 'treatment') continue;
            $query = "INSERT INTO subject_levels (subject_id, factor, level) " .
            "VALUES (###);";
            $params = array($id, $f, $l);
            $query = $this->db->replace_params($query, $params);
            if(!$this->db->queryExec($query, $error))
            {
              $this->restore_trial_or_die($error);
            }
        }
        $subject['id'] = $id;
        $subject['enrolled_by'] = $user_name;
        $subject['date_enrolled'] = $d;
        $subject['date_modified'] = $d;
        $this->subjects[$id] = $subject;
        $this->status = 'Enroll';
        return $this->subjects[$id];
    }

    function update_subject($id, $subject) {
        $treatment = $subject['treatment'];
        unset($subject['treatment']);
        $d = date('Y-m-d H:i:s');
        $query = "UPDATE subjects SET treatment=%s, date_modified=%s WHERE id=%s;";
        $params = array($treatment, $d, $id);
        $params = array_map(array($this->db, 'quote'), $params);
        $query = vsprintf($query, $params);
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $query = "DELETE from subject_levels WHERE subject_id=%s;";
        $query = sprintf($query, $this->db->quote($id));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        foreach($subject as $f => $l) {
            if ($f == 'treatment') continue;
            $query = "INSERT INTO subject_levels (subject_id, factor, level) " .
            "VALUES (###);";
            $params = array($id, $f, $l);
            $query = $this->db->replace_params($query, $params);
            if(!$this->db->queryExec($query, $error))
            {
              $this->restore_trial_or_die($error);
            }
        }
        $subject['id'] = $id;
        $subject['treatment'] = $treatment;
        $subject['date_modified'] = $d;
        $this->subjects[$id] = $subject;
        $this->update_frequencies();
    }

    function db_query($query, $assoc, $err_prefix) {
        $conn = $this->get_db_conn();
        if($result = $conn->query($query, $assoc, $error)) {
            return $result;
        } else {
            $this->restore_trial_or_die($err_prefix . '<br />' . $error);
        }
    }

    function save_as_preload() {
        $query = "DELETE FROM subjects;";
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error . ' delete subject');
        }
        $query = "DELETE FROM subject_levels;";
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error . ' delete subject');
        }
        $this->subjects = array();
        $this->update_preload($this->frequencies);
        $this->set_status();
    }

    function delete_subject($id, $delete_id=false) {
        $query = "DELETE FROM subjects WHERE id=%s;";
        $query = sprintf($query, $this->db->quote($id));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error . ' delete subject');
        }
        $query = "DELETE FROM subject_levels WHERE subject_id=%s;";
        $query = sprintf($query, $this->db->quote($id));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error . ' delete subject');
        }
        unset($this->subjects[$id]);
        $this->set_status();
        if (! $delete_id) return;
        $query = "DELETE FROM used_ids WHERE id=%s;";
        $query = sprintf($query, $this->db->quote($id));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error . ' delete id');
        }
    }
    
    function update_sample_size($new_size) {
        $query = "UPDATE settings SET sample_size=%s;";
        $query = sprintf($query, $this->db->quote($new_size));
        if(!$this->db->queryExec($query, $error))
        {
          $this->restore_trial_or_die($error);
        }
        $this->sample_size = $new_size;
        $this->update_subjects_ids();
    }

    function update_subjects_ids() {
        foreach($this->subjects as $id => $subject) {
            switch ($this->id_type) {
                case "Numeric":
                    $l = strlen((string) $this->sample_size);
                    $new_id = str_pad($id, $l, "0", STR_PAD_LEFT);
                    break;
                case "Alpha":
                    $l = ceil(log($this->sample_size, 26));
                    $new_id = str_pad($id, $l, "A", STR_PAD_LEFT);
                    break;
                case "Alphanumeric":
                    $l = ceil(log($this->sample_size+1, 36));
                    $new_id = str_pad($id, $l, "0", STR_PAD_LEFT);
                    break;
            }
            if ($new_id !== $id) {
                $query = "UPDATE subjects SET id=%s WHERE id=%s;";
                $params = array($new_id, $id);
                $params = array_map(array($this->db, 'quote'), $params);
                $query = vsprintf($query, $params);
                if(!$this->db->queryExec($query, $error))
                {
                  $this->restore_trial_or_die($error);
                }
                $query = "UPDATE subject_levels SET subject_id=%s WHERE subject_id=%s;";
                $params = array($new_id, $id);
                $params = array_map(array($this->db, 'quote'), $params);
                $query = vsprintf($query, $params);
                if(!$this->db->queryExec($query, $error))
                {
                  $this->restore_trial_or_die($error);
                }
            }
        }
    }

    function pad_id_numeric($n) {
        $l = strlen((string) $this->sample_size);
        return str_pad((string) $n, $l, "0", STR_PAD_LEFT);
    }

    function convert_to_alpha($n) {
        $n -= 1;
        $c = strtoupper(base_convert((string) $n, 10, 26));
        $tr = array_combine(range('P', 'A'), range('Z', 'K'));
        $c = strtr($c, $tr);
        $tr = array_combine(range('9', '0'), range('J', 'A'));
        $c = strtr($c, $tr);
        $l = ceil(log($this->sample_size, 26));
        $c = str_pad($c, $l, "A", STR_PAD_LEFT);
        return $c;
    }

    function convert_to_alphanumeric($n) {
        $c = strtoupper(base_convert((string) $n, 10, 36));
        $l = ceil(log($this->sample_size+1, 36));
        return str_pad($c, $l, "0", STR_PAD_LEFT);
    }

    function get_db_conn() {
        try {
            return new SQLiteDatabase($this->db_file, 0666, $error);
        } catch (Exception $e) {
            $this->restore_trial_or_die($error);
        }
    }

    function create_db() {
        $sql = file_get_contents(BASE_DIR . 'new_trial.sql');
        if (!$this->db->queryExec($sql, $error))
            die($error);
        $d = date('Y-m-d H:i:s');
        $query = 'INSERT INTO settings (date_initiated) VALUES (%s)';
        $query = sprintf($query, $this->db->quote($d));
        if(!$this->db->queryExec($query, $error))
        {
          die($error);
        }
    }

    function init_db() {
        $assoc = $this->user['assoc'];
        if (isset($_SESSION['db_file']))
            $db_file = $_SESSION['db_file'];
        else
            $db_file = FILES_DIR . "trial_" . $this->user['email'] . "_${assoc}.sqlite";
        $_SESSION['db_file'] = $db_file;
        $this->db_file = $db_file;
        try {
            $file_exist = file_exists($db_file);
            $this->db =& DB::getTrialDB($this->db_file);
            if (!$file_exist)
                $this->create_db();
            $query = "SELECT date_initiated FROM settings;";
            if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
                $row = $result->fetch();
                $this->date_initiated = $row['date_initiated'];
            } else {
                die($error);
            }
        } catch (Exception $e) {
            $this->restore_trial_or_die($error);
        }
    }

    /* If a backup file exists deletes current trial file and restore trial
     * from it.
     */
    function restore_trial_or_die($error, $suffix='.bak') {
        die($error);
        if (file_exists($this->db_file . $suffix)) {
            if (file_exists($this->db_file))
                unlink($this->db_file);
            copy($this->db_file . $suffix, $this->db_file);
            $error_log = date("[F j, Y, g:i:s a] ") . $this->db_file . ":". $error;
            $trial_log = '';
            if (file_exists(FILES_DIR . 'trial.log'))
                $trial_log = file_get_contents(FILES_DIR . 'trial.log') . "\n";
            $trial_log .= $error_log;
            file_put_contents(FILES_DIR . 'trial.log', $trial_log);
            die($error);
            $this->restored = true;
            $this->init_trial();
      } else {
            die('custom');
        }
    }

    function get_treatments_summary() {
        $ret = array();
        foreach ($this->treatments as $treatment => $ratio) {
            $ret[] = "$treatment => $ratio";
        }
        return "Treatments: " . implode(', ', $ret);
    }

    function get_factors_summary() {
        $ret = array();
        foreach ($this->factors as $factor => $value) {
            $ret[] = "$factor => " . $value[0] . ", (" . implode(', ', $value[1]) . ")";
        }
        return "Factors: " . implode('; ', $ret);
    }

    function get_summary() {
        if (!$this->treatments || !$this->factors) return false;
        $ret = array();
        $ret[] = $this->get_treatments_summary();
        $ret[] = $this->get_factors_summary();
        $ret[] = "Probability Method: " . $this->prob_method;
        $ret[] = "Base Probability: " . $this->base_prob;
        $ret[] = "Distance Method: " . $this->dist_method;
        $ret[] = "Sample Size: " . $this->sample_size;
        $ret[] = "Identifier Type: " . $this->id_type;
        $ret[] = "Identifier Order: " . $this->id_order;
        $ret[] = "Number of subjects: " . count($this->subjects);
        $ret[] = "Date Initiated: " . $this->date_initiated;
        return $ret;
    }

    function get_trial_balance() {
        $levels = array();
        $balance = array();
        foreach ($this->frequencies as $treatment => $factors) {
            foreach ($factors as $factor => $ls) {
                $levels[$factor] = array();
                foreach ($ls as $level => $v) {
                    $levels[$factor][$level] = array();
                }
            }
        }
        foreach ($levels as $factor => $ls) {
            $balance[$factor] = array();
            $balance[$factor]['total'] = array('Marginal Balance' => 0, 'Range' => 0, 'Variance' => 0, 'SD' => 0);
            foreach ($ls as $level => $v) {
                $balance[$factor][$level] = array();
                foreach ($this->frequencies as $treatment => $factors) {
                    $levels[$factor][$level][$treatment] = 1.0 * $this->frequencies[$treatment][$factor][$level] / $this->treatments[$treatment];
                }
            }
        }
        $means = array('Marginal Balance' => 0, 'Range' => 0, 'Variance' => 0, 'SD' => 0);
        $maxes = array('Marginal Balance' => 0, 'Range' => 0, 'Variance' => 0, 'SD' => 0);
        $m = 0;
        foreach ($levels as $factor => $ls) {
            foreach ($ls as $level => $cnt) {
                $mb = $this->get_marginal_balance($cnt);
                $mb *= $this->factors[$factor][0];
                $balance[$factor][$level]['Marginal Balance'] = $mb;
                $rng = max(array_values($cnt)) - min(array_values($cnt));
                $rng *= $this->factors[$factor][0];
                $balance[$factor][$level]['Range'] = $rng;
                $variance = $this->get_variance($cnt);
                $variance *= $this->factors[$factor][0];
                $balance[$factor][$level]['Variance'] = $variance;
                $sd = $this->get_standard_deviation($cnt);
                $sd *= $this->factors[$factor][0];
                $balance[$factor][$level]['SD'] = $sd;
                foreach ($balance[$factor][$level] as $stat => $statv) {
                    $means[$stat] += $balance[$factor][$level][$stat];
                    if ($maxes[$stat] < $balance[$factor][$level][$stat]) {
                        $maxes[$stat] = $balance[$factor][$level][$stat];
                    }
                    $balance[$factor]['total'][$stat] += $balance[$factor][$level][$stat];
                }
                $m++;
            }
            foreach ($balance[$factor]['total'] as $stat => $statv) {
                $balance[$factor]['total'][$stat] /= (1.0 * count($levels[$factor]));
            }
        }
        foreach ($means as $stat => $statv) {
            $means[$stat] /= (1.0 * $m);
        }
        $balance['Mean'] = $means;
        $balance['Max'] = $maxes;
        return $this->format_balance($balance);
    }

    function format_balance($balance) {
        $table = array();
        foreach($this->factors as $factor => $v) {
            $row = array($factor, '');
            $factor_balance = $balance[$factor];
            $total_balance = $factor_balance['total'];
            $row[] = $total_balance['Marginal Balance'];
            $row[] = $total_balance['Range'];
            $row[] = $total_balance['Variance'];
            $row[] = $total_balance['SD'];
            $table[] = $row;
            foreach($v[1] as $level) {
                $row = array('', $level);
                $level_balance = $factor_balance[$level];
                $row[] = $level_balance['Marginal Balance'];
                $row[] = $level_balance['Range'];
                $row[] = $level_balance['Variance'];
                $row[] = $level_balance['SD'];
                $table[] = $row;
            }
        }
        $row = array('Mean', '');
        $mean_balance = $balance['Mean'];
        $row[] = $mean_balance['Marginal Balance'];
        $row[] = $mean_balance['Range'];
        $row[] = $mean_balance['Variance'];
        $row[] = $mean_balance['SD'];
        $table[] = $row;
        $row = array('Max', '');
        $max_balance = $balance['Max'];
        $row[] = $max_balance['Marginal Balance'];
        $row[] = $max_balance['Range'];
        $row[] = $max_balance['Variance'];
        $row[] = $max_balance['SD'];
        $table[] = $row;
        return $table;
    }

    function get_grand_total($load) {
       $grand_total = array();
        foreach ($this->factors as $factor => $value) {
            foreach ($value[1] as $level) {
                $grand_total[$factor . '-' . $level] = 0;
            }
        }
        $grand_total['total'] = 0;
        foreach ($load as $treatment => $factors) {
            foreach ($factors as $factor => $levels) {
                $s = array_sum($levels);
                foreach ($levels as $level_name => $level_value) {
                    $grand_total[$factor . '-' . $level_name] += $level_value;
                }
            }
            $grand_total['total'] += $s;
        }
        return $grand_total;
    }

    function get_marginal_balance($cnt) {
        $combinatorics = new Math_Combinatorics;
        $com_counts = $combinatorics->combinations($cnt, 2);
        $a = array();
        foreach ($com_counts as $com_count) {
            $c = array_values($com_count);
            $a[] = abs($c[0] - $c[1]);
        }
        $numerator = array_sum($a);
        $denominator = (count($cnt)-1) * array_sum(array_values($cnt));
        if ($denominator == 0) return 0.0;
        return (1.0 * $numerator) / $denominator;
    }
    
    function get_variance($cnt) {
        $mean = (1.0 * array_sum(array_values($cnt))) / count($cnt);
        $sq_terms_arr = array();
        foreach(array_values($cnt) as $i) {
            $sq_terms_arr[] = pow(($i-$mean), 2);
        }
        $sq_terms = array_sum($sq_terms_arr);
        return (1.0 * $sq_terms) / (count($cnt) - 1.0);
    }
    
    function get_standard_deviation($cnt) {
        return sqrt($this->get_variance($cnt));
    }    

    function get_num_selected_subjects($selected_treatments, $selected_factors, $selected_id){
        $q1 = array();
        $q2 = array();
        $w = "WHERE subjects.treatment IN ($selected_treatments)";
        foreach ($this->factors as $f => $v) {
            $q1[] = "'$f'.level AS '$f'";
            $q2[] = "LEFT JOIN subject_levels '$f' ON ('$f'.subject_id = subjects.id AND '$f'.factor = '$f')";
            $selected_factor = $selected_factors[$f];
            $w .= " AND '$f'.level IN ($selected_factor)";
        }
        if (strtolower($selected_id) != 'all')
            $w = "WHERE subjects.id = " . $this->db->quote($selected_id);
        $q1 = implode(', ', $q1);
        $q2 = implode(' ', $q2);
        $query = "SELECT DISTINCT subjects.*, $q1 FROM subjects $q2 $w;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            return $result->numRows();
        } else {
            $this->restore_trial_or_die($error);
        }
    }

    function get_subjects_limited($offset, $rowsperpage, $selected_treatments, $selected_factors, $selected_id) {
        $subjects = array();
        $q1 = array();
        $q2 = array();
        $w = "WHERE subjects.treatment IN ($selected_treatments)";
        foreach ($this->factors as $f => $v) {
            $q1[] = "'$f'.level AS '$f'";
            $q2[] = "LEFT JOIN subject_levels '$f' ON ('$f'.subject_id = subjects.id AND '$f'.factor = '$f')";
            $selected_factor = $selected_factors[$f];
            $w .= " AND '$f'.level IN ($selected_factor)";
        }
        if (strtolower($selected_id) != 'all')
            $w = "WHERE subjects.id = " . $this->db->quote($selected_id);
        $q1 = implode(', ', $q1);
        $q2 = implode(' ', $q2);
        $query = "SELECT DISTINCT subjects.id AS id, subjects.treatment AS treatment, subjects.enrolled_by AS enrolled_by, subjects.date_enrolled AS date_enrolled, subjects.date_modified AS date_modified, $q1 FROM subjects $q2 $w LIMIT $offset, $rowsperpage;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $subjects[$row['id']] = (array) $row;
            }
        } else {
            $this->restore_trial_or_die($error);
        }
        return $subjects;
    }

    public static function &getTrial($user, $refresh=false) {
        static $instance;
		if (!isset($instance) or $refresh) {
			$instance = new Trial($user);
		}
		return $instance;
    }

    public function export_sql()
    {
        $tables = array('factors', 'levels', 'preload', 'settings', 'subject_levels', 'subjects', 'treatments');
        $query = "SELECT * FROM sqlite_master WHERE type='table'";
        $result = $this->db->query($query, SQLITE_ASSOC, $error);

        $ret_sql = array();
        $ret_sql[] = "BEGIN TRANSACTION";

        //iterate through each table
        while($row = $result->fetch())
        {
            $valid = false;
            for($j=0; $j<sizeof($tables); $j++)
            {
                if($row['tbl_name']==$tables[$j])
                    $valid = true;
            }
            if($valid)
            {
                $ret_sql[] = $row['sql'];
                if($row['type']=="table")
                {
                    $query = "SELECT * FROM ".$this->db->quote_id($row['tbl_name']);
                    $arr = $this->db->query($query, SQLITE_ASSOC, $error);

                    $query = "PRAGMA table_info(".$this->db->quote_id($row['tbl_name']).")";
                    $temp = $this->db->query($query, SQLITE_NUM, $error);
                    $cols = array();
                    $cols_quoted = array();
                    $vals = array();
                    $z = 0;
                    while($temp_row = $temp->fetch())
                    {
                        $cols[$z] = $temp_row[1];
                        $cols_quoted[$z] = $this->db->quote_id($temp_row[1]);
                        $z++;
                    }
                    $z = 0;
                    while($arr_row = $arr->fetch())
                    {
                        for($y=0; $y<sizeof($cols); $y++)
                        {
                            if(!isset($vals[$z]))
                                $vals[$z] = array();
                            if($arr_row[$cols[$y]] === NULL)
                                $vals[$z][$cols[$y]] = 'NULL';
                            else
                                $vals[$z][$cols[$y]] = $this->db->quote($arr_row[$cols[$y]]);
                        }
                        $z++;
                    }
                    for($j=0; $j<sizeof($vals); $j++)
                        $ret_sql[] = "INSERT INTO ".$this->db->quote_id($row['tbl_name'])." (".implode(",", $cols_quoted).") VALUES (".implode(",", $vals[$j]).")";
                }
            }
        }
        $ret_sql[] = "COMMIT";
        return implode(";\r\n", $ret_sql);
    }

	//export csv
	public function export_csv()
	{
	    $field_terminate = ';';
	    $field_enclosed = '"';
	    $field_escaped = chr(92);
	    $null = 'NULL';
        $tables = array('factors', 'levels', 'preload', 'settings', 'subject_levels', 'subjects', 'treatments');
		$query = "SELECT * FROM sqlite_master WHERE type='table' or type='view' ORDER BY type DESC";
		$result = $this->db->query($query, SQLITE_ASSOC, $error);
		$nurrow = $result->numRows();
		$csv_str = "";
		$i = 0;
		while($row = $result->fetch())
		{
			$valid = false;
			for($j=0; $j<sizeof($tables); $j++)
			{
				if($row['tbl_name']==$tables[$j])
					$valid = true;
			}
			if($valid)
			{
			    $csv_str .= "TABLE:" . $row['tbl_name'] . "\r\n";
				$query = "PRAGMA table_info(".$this->db->quote_id($row['tbl_name']).")";
				$temp = $this->db->query($query, SQLITE_NUM, $error);
				$cols = array();
                $z = 0;
                while($temp_row = $temp->fetch()) {
					$cols[$z] = $temp_row[1];
					$z++;
				}
				for($z=0; $z<sizeof($cols); $z++)
				{
					$csv_str .= $field_enclosed . $cols[$z] . $field_enclosed;
					// do not terminate the last column!
					if($z < sizeof($cols)-1)
						$csv_str .= $field_terminate;
				}
				$csv_str .= "\r\n";
				$query = "SELECT * FROM " . $this->db->quote_id($row['tbl_name']);
				$arr = $this->db->query($query, SQLITE_ASSOC, $error);
				$arrrows = $arr->numRows();
                $z = 0;
                while($arr_row = $arr->fetch())
				{
					for($y=0; $y<sizeof($cols); $y++)
					{
						$cell = $arr_row[$cols[$y]];
						$cell = str_replace($field_terminate, $field_escaped . $field_terminate, $cell);
						$cell = str_replace($field_enclosed, $field_escaped . $field_enclosed, $cell);
						// do not enclose NULLs
						if($cell == NULL)
							$csv_str .= $null;  
						else
							$csv_str .= $field_enclosed . $cell . $field_enclosed;
						// do not terminate the last column!
						if($y < sizeof($cols)-1)
							$csv_str .= $field_terminate;
					}
					if($z<$arrrows-1)
						$csv_str .= "\r\n";	
					$z++;
				}
				if($i<$nurrow-1)
					$csv_str .= "\r\n";
			}
			$i++;
		}
		return $csv_str;
	}

}
?>
