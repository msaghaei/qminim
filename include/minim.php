<?php

/**
 * @file minim.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 
define('MARGINAL_BALANCE', 'Marginal Balance');
define('RANGE', 'Range');
define('STANDARD_DEVIATION', 'Standard Deviation');
define('VARIANCE', 'Variance');
define('BIASED_COIN', 'Biased Coin');
define('NAIVE', 'Naive');
define('EPSILON', 1.0E-5);

require_once('Combinatorics.php');

class Minim {
    
    var $treatments;
    var $factors;
    var $prob_method;
    var $dist_method;
    var $base_prob;
    var $preload;
    var $subjects;
    var $minimum_treatment;

    function Minim(&$trial) {
        mt_srand(time());
        $this->trial = $trial;
    }
    
    function enroll(&$subjects, $multi=false) {
        $level_count = array();
        foreach ($this->trial->factors as $f => $v) {
            $level_count[$f] = array();
            foreach ($this->trial->treatments as $t => $r) {
                $level_count[$f][$t] = 0;
            }
        }
        for ($idx=0; $idx<count($subjects);$idx++) {
            $subject = $subjects[$idx];
            foreach ($this->trial->frequencies as $t => $factors) {
                foreach ($this->trial->factors as $f => $v) {
                    $l = $subject[$f];
                    $level_count[$f][$t] = $this->trial->frequencies[$t][$f][$l];
                }
            }
            $scores = array();
            foreach ($this->trial->treatments as $t => $r) {
                $scores[$t] = 0;
                foreach ($this->trial->factors as $f => $v) {
                    $w = $v[0];
                    $i = $this->get_imbalance_score($level_count[$f], $t);
                    $scores[$t] += 1.0 * $w * $i;
                }
            }
            $min_treatments = $this->get_min_ties($scores);
            if (count($min_treatments) == count($this->trial->treatments)) {
                // all treatment have same score
                // so build a probs based on treatment ratios
                $row_probs = array();
                foreach ($this->trial->treatments as $treatment => $ratio) {
                    $row_probs[$treatment] = $ratio;
                }
            } else {
                // ids of prefered treatment(s)
                // randomly selecting an id
                $pt_key = array_rand($min_treatments);
                $pt = $min_treatments[$pt_key];
                $row_probs = $this->probs[$pt];
            }
            $t = $this->get_rand_biased($row_probs);
            foreach ($this->trial->factors as $f => $v) {
                $l = $subject[$f];
                $this->trial->frequencies[$t][$f][$l]++;
            }
            $subject['treatment'] = $t;
            $subjects[$idx] = $subject;
        }

    }

    function uniform() {
        return mt_rand() / mt_getrandmax();
    }

    function get_rand_biased($probs) {
        // probs: [treatment1 => prob, treatment2 => prob, ...}
        $p = $this->uniform() * array_sum($probs);
        $sum_probs = 0;
        foreach ($probs as $treatment => $v) {
            $sum_probs += $v;
            if ($p < $sum_probs) return $treatment;
        }
    }

    function build_probs() {
        $minimum_treatment = $this->minimum_treatment;
        $base_prob = $this->base_prob;
        $this->probs = array();
        foreach ($this->trial->treatments as $id1 => $r1) {
            $Ps = array();
            foreach ($this->trial->treatments as $id2 => $r2) {
                $Ps[$id2] = 0;
            }
            $this->probs[$id1] = $Ps;
        }
        $this->probs[$minimum_treatment][$minimum_treatment] = $base_prob;
        foreach ($this->trial->treatments as $id1 => $r1) {
            if ($id1 == $minimum_treatment) continue;
            // $this->trial->treatments[$id1] is the ratio for treatment with id = row
            $a = array();
            foreach ($this->trial->treatments as $id2 => $r2) {
                if ($id1 != $id2) $a[] = $r2;
            }
            $numerator = array_sum($a);
            $a = array();
            foreach ($this->trial->treatments as $id2 => $r2) {
                if ($id2 != $minimum_treatment) $a[] = $r2;
            }
            $denominator = array_sum($a);
            if ($this->prob_method == BIASED_COIN) {
                $this->probs[$id1][$id1] = 1.0 - (1.0 * $numerator / $denominator) * (1.0 - $base_prob);
            } elseif ($this->prob_method == NAIVE) {
                $this->probs[$id1][$id1] = $base_prob;
            }
        }
        foreach ($this->trial->treatments as $id1 => $r1) {
            foreach ($this->trial->treatments as $id2 => $r2) {
                if ($id1 == $id2) continue;
                $H = $this->probs[$id1][$id1];
                $numerator = $r2;
                $a = array();
                foreach ($this->trial->treatments as $id3 => $r3) {
                    if ($id3 != $id1) $a[] = $r3;
                }
                $denominator = array_sum($a);
                if ($this->prob_method == BIASED_COIN) {
                    $this->probs[$id1][$id2] = (1.0 * $numerator / $denominator) * (1.0 - $H);
                } elseif ($this->prob_method == NAIVE) {
                    $this->probs[$id1][$id2] = 1.0 * (1.0 - $base_prob) / (count($this->trial->treatments) - 1.0);
                }
            }
        }
    }


    function get_min_ties($scores) {
        $min_score = min(array_values($scores));
        $ret = array();
        foreach ($scores as $treatment => $value) {
            // this pair is equal
            if (abs($value - $min_score) < EPSILON) {
                // so take it
                $ret[] = $treatment;
            }
        }
        return $ret;
    }
    
    function get_imbalance_score($cnt, $initial_treatment) {
        $cnt[$initial_treatment] += 1;
        $adj_count = array();
        foreach ($cnt as $t => $c) {
            $adj_count[$t] = (1.0 * $c) / $this->trial->treatments[$t];
        }
        switch ($this->dist_method) {
            case MARGINAL_BALANCE:
                $ret = $this->trial->get_marginal_balance($adj_count);
                break;
            case RANGE:
                $ret = max(array_values($adj_count)) - min(array_values($adj_count));
                break;
            case VARIANCE:
                $ret = $this->trial->get_variance($adj_count);
                break;
            case STANDARD_DEVIATION:
                $ret = $this->trial->get_standard_deviation($adj_count);
                break;
        }
        $cnt[$initial_treatment] -= 1;
        return $ret;
    }
}

?>
