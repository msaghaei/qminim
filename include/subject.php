<?php

/**
 * @file subject.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function subject_added() {
    global $smarty;
    $trial = get_trial_instance();
    $save_msg = array();
    $subject = array();
    $a = array();
    $b = array();
    foreach ($trial->factors as $factor => $v) {
        $levels = $v[1];
        $level = strip_custom($_POST[$factor]);
        if (in_array($level, $levels)) {
            $subject[$factor] = $level;
        } else {
            $save_msg[] = "Select a value for ${factor}!";
        }
        $levels_arr = array_merge((array)'---------', (array)$levels);
        $a[$factor] = array_combine($levels_arr, $levels_arr);
        $b[$factor] = $level;
    }
    if (! $save_msg) {
        $subject = $trial->insert_subject($subject);
        $smarty->assign('subject', $subject);
        $smarty->assign('subjects', $trial->subjects);
        $smarty->assign("sample_size", $trial->sample_size);
        $smarty->assign('subject_keys', array_keys($subject));
        $smarty->display('subject_added.tpl');
    } else {
        $smarty->assign('save_msg', implode(', ', $save_msg));
        $smarty->assign('options_arr', $a);
        $smarty->assign('selects_arr', $b);
        $smarty->assign('factors', $trial->factors);
        $smarty->display('add_subject.tpl');
    }
}

function enroll_all() {
    $trial = get_trial_instance();
    $trial->enroll_all();
    display_index_page('subjects.tpl');
}

function subject_edited() {
    global $smarty;
    $trial = get_trial_instance();
    $subject = array();
    $id = strip_custom($_POST['id']);
    $subject['treatment'] = strip_custom($_POST['treatment']);
    foreach ($trial->factors as $factor => $v) {
        $level = strip_custom($_POST[$factor]);
        $subject[$factor] = $level;
    }
    $trial->update_subject($id, $subject);
    if (isset($_POST['currentpage']))
        $smarty->assign('currentpage', strip_custom($_POST['currentpage']));
    display_index_page('subjects.tpl');
}

function delete_subject() {
    global $smarty;
    $trial = get_trial_instance();
    $id = strip_custom($_POST['id']);
    $delete_id = false;
    if (isset($_POST['undolast']) && $_POST['undolast'] == 1)
        $delete_id = true;
    $trial->delete_subject($id, $delete_id);
    display_index_page('subjects.tpl');
}

function set_subjects_filter_from_freq() {
    $trial = get_trial_instance();
    $selected_treatments = array();
    $posted_treatment = $_POST['selected_treatment_freq'];
    foreach ($trial->treatments as $t => $r) {
        $selected_treatments[$t] = false;
    }
    if ($posted_treatment == "") {
        $sel_treatments = array();
        foreach ($trial->treatments as $t => $r) {
            $selected_treatments[$t] = true;
            $sel_treatments[] = $t;
        }
        $sel_treatments = '"' . implode('","', $sel_treatments) . '"';
    } else {
        $selected_treatments[$posted_treatment] = true;
        $sel_treatments = '"' . $posted_treatment . '"';
    }

    $posted_factor = $_POST['selected_factor_freq'];
    $posted_level = $_POST['selected_level_freq'];
    $selected_factors = array();
    $sel_factors = array();
    foreach ($trial->factors as $f => $v) {
        $selected_factor = array();
        $sel_factor = array();
        foreach ($v[1] as $l) {
            $selected_factor[$f . "_" . $l] = true;
            if ($f == $posted_factor && $l != $posted_level)
                $selected_factor[$f . "_" . $l] = false;
            if ($selected_factor[$f . "_" . $l])
                $sel_factor[] = $l;
        }
        $selected_factors[$f] = $selected_factor;
        $sel_factors[$f] = '"' . implode('","', $sel_factor) . '"';
    }
    $_SESSION['subjectsfilter']['selected_treatments'] = $selected_treatments;
    $_SESSION['subjectsfilter']['sel_treatments'] = $sel_treatments;
    $_SESSION['subjectsfilter']['selected_factors'] = $selected_factors;
    $_SESSION['subjectsfilter']['sel_factors'] = $sel_factors;    
    if (! isset($_SESSION['subjectsfilter']['include_preload']))
        set_include_preload();
}

function set_include_preload() {
    if (! isset($_POST['include_preload']))
        $include_preload = false;
    else
        $include_preload = $_POST['include_preload'];
    $_SESSION['subjectsfilter']['include_preload'] = $include_preload;
}

function set_subjects_filter() {
    $trial = get_trial_instance();
    $selected_treatments = array();
    $sel_treatments = array();
    foreach ($trial->treatments as $t => $r) {
        if (! isset($_POST["selected_treatments_$t"])) {
            $selected_treatments[$t] = true;
            $sel_treatments[] = $t;
        } else {
            if ($_POST["selected_treatments_$t"] == "selected") {
                $selected_treatments[$t] = true;
                $sel_treatments[] = $t;
            } else {
                $selected_treatments[$t] = false;
            }
        }
    }
    $sel_treatments = '"' . implode('","', $sel_treatments) . '"';
    $selected_factors = array();
    $sel_factors = array();
    foreach ($trial->factors as $f => $v) {
        $selected_factor = array();
        $sel_factor = array();
        foreach ($v[1] as $l) {
            $level = str_replace(' ', '$', $l);
            if (! isset($_POST["selected_factor_{$f}_{$level}"])) {
                $selected_factor[$f . "_" . $l] = true;
                $sel_factor[] = $l;
            } else {
                if ($_POST["selected_factor_{$f}_{$level}"] == "selected") {
                    $selected_factor[$f. "_" . $l] = true;
                    $sel_factor[] = $l;
                } else {
                    $selected_factor[$f. "_" . $l] = false;
                }
            }
        }
        $selected_factors[$f] = $selected_factor;
        $sel_factors[$f] = '"' . implode('","', $sel_factor) . '"';
    }
    $selected_id = 'All';
    if (isset($_POST['selected_id']) && $_POST['selected_id'])
        $selected_id = $_POST['selected_id'];
    $_SESSION['subjectsfilter']['selected_treatments'] = $selected_treatments;
    $_SESSION['subjectsfilter']['sel_treatments'] = $sel_treatments;
    $_SESSION['subjectsfilter']['selected_factors'] = $selected_factors;
    $_SESSION['subjectsfilter']['sel_factors'] = $sel_factors;
    $_SESSION['subjectsfilter']['selected_id'] = $selected_id;
    if (! isset($_SESSION['subjectsfilter']['include_preload']))
        set_include_preload();
}

?>
