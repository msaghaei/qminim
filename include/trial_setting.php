<?php

/**
 * @file trial_setting.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function new_sample_size() {
    global $smarty;
    $save_msg = array();
    $trial = get_trial_instance();
    $new_sample_size = strip_custom($_POST['new_sample_size']);
    if ((int) $new_sample_size <= $trial->sample_size || (int) $new_sample_size > Settings::getSetting('max_allowed_subjects'))
        $save_msg[] = "Invalid new sample size! Must be higher than original size (" . $trial->sample_size . ") and up to " . Settings::getSetting('max_allowed_subjects');
    if ($save_msg) {
        $smarty->assign("save_msg", implode(', ', $save_msg));
        $smarty->assign("new_sample_size", $new_sample_size);
    } else {
        $trial->sample_size = (int) $new_sample_size;
        $trial->update_sample_size((int) $new_sample_size);
        $trial = get_trial_instance(true);
    }
    display_index_page("settings.tpl");
}

function settings_edited() {
    global $smarty;
    $save_msg = array();
    $trial = get_trial_instance();
    $title = strip_custom($_POST['title']);
    $prob_method = strip_custom($_POST['prob_method']);
    $base_prob = strip_custom($_POST['base_prob']);
    $dist_method = strip_custom($_POST['dist_method']);
    $sample_size = strip_custom($_POST['sample_size']);
    $id_type = strip_custom($_POST['id_type']);
    $id_order = strip_custom($_POST['id_order']);
    $recycle_ids = 0;
    if (isset($_POST['recycle_ids']))
        $recycle_ids = 1;
    if ((float) $base_prob <= 0 || (float) $base_prob >= 1) {
        $save_msg[] = "Invalid base probability!";
    }
    if (count($trial->subjects)) {
        if ((int) $sample_size < count($trial->subjects) || (int) $sample_size > Settings::getSetting('max_allowed_subjects')) {
            $save_msg[] = "Invalid new sample size! Must be higher than number of already enrolled subjects (" . count($trial->subjects) . ") and up to ". Settings::getSetting('max_allowed_subjects');
        }
    } else {
        if ((int) $sample_size < 10 || (int) $sample_size > Settings::getSetting('max_allowed_subjects')) {
            $save_msg[] = "Invalid sample size! Must be between 10 and ". Settings::getSetting('max_allowed_subjects');
        }
    }
    if ($save_msg) {
        $smarty->assign("save_msg", implode(', ', $save_msg));
        $smarty->assign("prob_arr", array('Biased Coin' => 'Biased Coin', 'Naive' => 'Naive'));
        $smarty->assign("dist_arr", array('Marginal Balance' => 'Marginal Balance', 'Range' => 'Range', 'Standard Deviation' => 'Standard Deviation', 'Variance' => 'Variance'));
        $smarty->assign("id_type_arr", array('Numeric' => 'Numeric', 'Alpha' => 'Apha', 'Alphanumeric' => 'Alphanumeric'));
        $smarty->assign("id_order_arr", array('Sequential' => 'Sequential', 'Random' => 'Random'));
        $smarty->assign("prob_method", $prob_method);
        $smarty->assign("dist_method", $dist_method);
        $smarty->assign("base_prob", $base_prob);
        $smarty->assign("sample_size", $trial->sample_size);
        $smarty->assign("id_type", $trial->id_type);
        $smarty->assign("id_order", $trial->id_order);
        $smarty->assign("recycle_ids", $trial->recycle_ids);
        $smarty->assign("status", $trial->status);
        $smarty->display('edit_settings.tpl');
    } else {
        if (count($trial->subjects)) {
            $trial->update_settings($prob_method, (float) $base_prob, $dist_method, (int) $trial->sample_size, $id_type, $id_order, $title, (int) $recycle_ids);
            $trial->update_sample_size((int) $sample_size);
        } else {
            $trial->update_settings($prob_method, (float) $base_prob, $dist_method, (int) $sample_size, $id_type, $id_order, $title, (int) $recycle_ids);
        }
        $trial = get_trial_instance(true);
        $smarty->assign("status", $trial->status);
        display_index_page('settings.tpl');
    }
}

?>
