<?php

/**
 * @file treatment.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function treatment_added() {
    global $smarty;
    $trial = get_trial_instance();
    $save_msg = array();
    $treatment = $_POST['treatment'];
    if (! valid_identifier($treatment))
        $save_msg[] = "Invalid treatment name!";
    $ratio = (int) strip_custom($_POST['ratio']);
    if (!is_integer_numeric($ratio) or $ratio <= 0)
        $save_msg[] = "Invalid ratio!";
    if (array_key_exists($treatment, $trial->treatments))
        $save_msg[] = "Duplicate treatment!";
    if ($save_msg) {
        $save_msg = implode(', ', $save_msg);
        $smarty->assign('treatment', $treatment);
        $smarty->assign('ratio', $ratio);
        $smarty->assign('save_msg', $save_msg);
        $smarty->display('add_treatment.tpl');
    } else {
        $trial->insert_treatment($treatment, (int) $ratio);
        display_index_page('treatments.tpl');
    }
}

function treatment_edited() {
    global $smarty;
    $trial = get_trial_instance();
    $save_msg = array();
    $treatment = $_POST['treatment'];
    if (! valid_identifier($treatment))
        $save_msg[] = "Invalid treatment name!";
    $initialtreatment = strip_custom($_POST['initialtreatment']);
    $ratio = strip_custom($_POST['ratio']);
    if ((int) $ratio <= 0)
        $save_msg[] = "Invalid ratio!";
    if (array_key_exists($treatment, $trial->treatments) and $treatment != $initialtreatment)
        $save_msg[] = "Duplicate treatment!";
    if ($save_msg) {
        $save_msg = implode(', ', $save_msg);
        $smarty->assign('treatment', $treatment);
        $smarty->assign('initialtreatment', $initialtreatment);
        $smarty->assign('ratio', $ratio);
        $smarty->assign('save_msg', $save_msg);
        $smarty->display('edit_treatment.tpl');
    } else {
        $trial->update_treatment($initialtreatment, $treatment, $ratio);
        display_index_page('treatments.tpl');
    }
}

function delete_treatment() {
    global $smarty;
    $trial = get_trial_instance();
    $treatment = strip_custom($_POST['treatment']);
    $trial->delete_treatment($treatment);
    display_index_page('treatments.tpl');
}

?>