<?php

/**
 * @file admin.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html
 *
 */

if (!defined('GATE_PASSED')) exit(); 

function add_site_setting() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    list($name, $value, $type) = explode('/', $_POST['new_setting']);
    Settings::addSetting($name, $value, $type);
    display_site_settings();
}

function edit_site_setting() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    list($name, $value, $type) = explode('/', $_POST['new_setting']);
    Settings::updateSetting($_POST['setting_name'], $name, $value, $type);
    display_site_settings();
}

function delete_site_setting() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    Settings::deleteSetting($_POST['setting_name']);
    display_site_settings();
}

function change_setting_name() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    Settings::updateSettingName($_POST['setting_name'], $_POST['new_setting_name']);
    display_site_settings();
}

function change_setting_value() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    Settings::updateSettingValue($_POST['setting_name'], $_POST['new_setting_value']);
    display_site_settings();
}

function change_setting_type() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    Settings::updateSettingType($_POST['setting_name'], $_POST['new_setting_type']);
    display_site_settings();
}

function increase_admin_rank() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');

}

function admin_toggle() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    Settings::adminToggle($_POST['user_name']);
    show_site_users();
}

function display_site_settings() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $admin_user_names = Settings::getAdmins();
    $admins = array();
    foreach($admin_user_names as $user_name => $rank) {
        $admin = UserDAO::getUserByUsername($user_name);
        $admin['rank'] = $rank;
        $admins[] = $admin;
    }
    $smarty->assign('admins', $admins);
    $smarty->assign('settings', Settings::getSettings());
    $smarty->display('site_settings.tpl');
}

function manage_added_user() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $err_msg = array();
    $query = "SELECT COUNT(*) FROM users WHERE email = %s;";
    $db = get_users_db();
    $query = sprintf($query, $db->quote($_POST['email']));
    $del_req_email = false;
    if($result = $db->query($query, SQLITE_NUM, $error)) {
        if ($row = $result->fetch()) {
            if ($row[0] > 0 && $_POST['user_source'] == 'new') {
                $err_msg[] = "This email is in use!";
            } elseif ($row[0] > 0 && $_POST['user_source'] == 'requested') {
                $del_req_email = true;
            }
        }
    } else {
        die($error);
    }
    $query = "SELECT COUNT(*) FROM users WHERE user_name = %s;";
    $query = sprintf($query, $db->quote($_POST['user_name']));
    if($result = $db->query($query, SQLITE_NUM, $error)) {
        if ($row = $result->fetch()) {
            if ($row[0] > 0)
                $err_msg[] = "Username already in use. Select another username!";
        }
    } else {
        die($error);
    }

    if ($err_msg) {
        $smarty->assign('post', $_POST);
	    $smarty->assign('salutations', get_salutation());
	    $smarty->assign('job_titles', get_job_titles());
	    $smarty->assign('countries', get_countries());
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        $smarty->display('add_user.tpl');
    } else {
        if ($del_req_email) {
            delete_request_email();
        }
        extract($_POST);
        $date_registered = date('Y-m-d H:i:s');
        $jobs = get_job_titles();
        $job_title = $jobs[$job_title];
        $countries = get_countries();
        $country = $countries[$country];
        $password = hasher($password . $user_name);
        $assoc = mt_rand(10000000,100000000);

        $query = "INSERT INTO users (salutation, job_title, first_name, last_name, email, user_name, password, affiliation, country, city, comment, assoc, date_registered) VALUES (###);";
        $params = array($salutation, $job_title, $first_name, $last_name, $email, $user_name, $password, $affiliation, $country, $city, 'Added by the site admin', $assoc, $date_registered);
        $query = $db->replace_params($query, $params);
        if (!$db->queryExec($query, $error))
            die($error);
        $body = "Dear {$salutation} {$first_name} {$last_name}\n\n";
        $body .= "You have been registered with QMinim Online Minimisation, by the site admin. Use the following user name and password to log into the QMInim:\n\nUsername: {$user_name}\nPassword: " . $_POST['password'] . "\n\nPlease feel free to edit your profile or change your password\n\n";
        $body .= "QMinim Site Admin\n";
        $body .= $_SESSION['userloggedin']['email'] . "\n\n";
        $body .= "If this email is not related to you please ignore it, or inform the site admin\n\n";
        $subject = 'Registration with QMinim: Online Minimisation';
        $show_email_form = false;
        $params = array();
        if (isset($_POST['show_email_form']) and $_POST['show_email_form']) {
            $show_email_form = true;
            $params = array('user_name' => $user_name);
        }
        send_mail($subject, $body, $_POST['email'], $show_email_form, $params);
        if (!$show_email_form)
            show_site_users();
    }
}

function manage_message_sent() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');

    if ($_POST['user_email'] == 'all') {
        $users = array();
        $query = "SELECT email FROM users WHERE email IS NOT NULL;";
        $db = get_users_db();
        if($result = $db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $email = $row['email'];
                send_mail($_POST['message_subject'], $_POST['message_body'], $email);
            }
        }
    } else {
        send_mail($_POST['message_subject'], $_POST['message_body'], $_POST['user_email']);
    }
    show_site_users();
}

function manage_send_message() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $smarty->assign('post', $_POST);
    display_index_page('send_message.tpl');
}

function manage_logout_as() {
    global $smarty;
    $admin_user_name = $_SESSION['loginas'];
    if (!Settings::isAdmin($admin_user_name))
        header("Location: " . BASE_URL . 'index.php');
    $query = "SELECT * FROM users WHERE user_name = %s;";
    $db = get_users_db();
    $query = sprintf($query, $db->quote($admin_user_name));
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        if ($row = $result->fetch()) {
            unset($_SESSION['loginas']);
            unset($row['password']);
            $smarty->clear_all_assign();
            $smarty->clear_all_cache();
            $smarty->clear_compiled_tpl();
            session_unset();
            $_SESSION['userloggedin'] = (array) $row;
            $_SESSION['widescreen'] = $row['wide_screen'];
            if (Settings::isAdmin($row['user_name'])) {
                $_SESSION['userloggedin']['is_admin'] = true;
                $_SESSION['userloggedin']['rank'] = Settings::getAdminRank($row['user_name']);
            }
            header("Location: " . BASE_URL . 'index.php');
        }
    } else {
        die($error);
    }
}

function manage_login_as() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $admin_user_name = $_SESSION['userloggedin']['user_name'];
    $user_name = $_POST['user_name'];
    $query = "SELECT * FROM users WHERE user_name = %s;";
    $db = get_users_db();
    $query = sprintf($query, $db->quote($user_name));
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        if ($row = $result->fetch()) {
            unset($row['password']);
            $smarty->clear_all_assign();
            $smarty->clear_all_cache();
            $smarty->clear_compiled_tpl();
            session_unset();
            $_SESSION['userloggedin'] = (array) $row;
            $_SESSION['widescreen'] = $row['wide_screen'];
            $_SESSION['loginas'] = $admin_user_name;
            if (Settings::isAdmin($row['user_name'])) {
                $_SESSION['userloggedin']['is_admin'] = true;
                $_SESSION['userloggedin']['rank'] = Settings::getAdminRank($row['user_name']);
            }
            header("Location: " . BASE_URL . 'index.php');
        }
    } else {
        die($error);
    }
}

function repair_password() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $db = get_users_db();
    $user_name = $_POST['user_name'];
    $password = $_POST['password'];
    $hasher_pw = hasher($password . $user_name);
    $sql = "UPDATE users SET password=%s WHERE user_name=%s;";
    $params = array($hasher_pw, $user_name);
    $params = array_map(array($db, 'quote'), $params);
    $sql = vsprintf($sql, $params);
    if (!$db->queryExec($sql, $error))
        die($error);
    else
        show_site_users();
}

function change_user_name() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $err_msg = '';
    $user_name = $_POST['user_name'];
    $new_user_name = $_POST['new_user_name'];
    $check_user_name = true;
    if ($new_user_name == $user_name) {
        $check_user_name = false;
    }
    $db = get_users_db();
    if ($check_user_name) {
	    $query = "SELECT COUNT(*) FROM users WHERE user_name = %s;";
        $query = sprintf($query, $db->quote($new_user_name));
	    if($result = $db->query($query, SQLITE_NUM, $error)) {
	        if ($row = $result->fetch()) {
	            if ($row[0] > 0) {
	                $err_msg = $new_user_name . " is in use!";
	            }
	        }
	    } else {
	        die($error);
	    }
    }
    if ($err_msg) {
        $smarty->assign("err_msg", '<div class="error">' . $err_msg . '</div>');
        $smarty->assign("changing_user_name", $user_name);
    } else {
        $new_password = random_pw();
        $hasher_pw = hasher($new_password . $new_user_name);
        $sql = "UPDATE users SET user_name=%s, password=%s WHERE user_name=%s;";
        $params = array($new_user_name, $hasher_pw, $user_name);
        $params = array_map(array($db, 'quote'), $params);
        $sql = vsprintf($sql, $params);
        if (!$db->queryExec($sql, $error))
            die($error);
        $body = "Dear QMinim user\n\n";
        $body .= "Your usename and your password were changed to the following\n\n{$new_user_name}\n{$new_password}\n\nPlease login and change your password as soon as possible!\n\n";
        $body .= "QMinim Site Admin\n";
        $body .= $_SESSION['userloggedin']['email'] . "\n\n";
        $subject = 'Username and password changed: QMinim Online Minimisation';
        send_mail($subject, $body, $_POST['email']);
    }
    show_site_users();
}

function manage_user_edited() {
    global $smarty;
    $err_msg = '';
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $user_name = $_POST['user_name'];
    $query = "SELECT * FROM users WHERE email = %s;";
    $db = get_users_db();
    $query = sprintf($query, $db->quote($_POST['email']));
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        if ($row = $result->fetch()) {
            if ($row['user_name'] != $user_name) {
                $err_msg = $_POST['email'] . " is in use!";
            }
        }
    } else {
        die($error);
    }
    if ($err_msg) {
        $user = $row;
        unset($user['password']);
        $smarty->assign("err_msg", '<div class="error">' . $err_msg . '</div>');
        $smarty->assign('salutations', get_salutation());
        $smarty->assign('job_titles', get_job_titles());
        $smarty->assign('countries', get_countries());
        $smarty->assign('post', $_POST);
        $smarty->display('edit_user.tpl');
        return;
    }
    extract($_POST);
    $jobs = get_job_titles();
    $job_title = $jobs[$job_title];
    $countries = get_countries();
    $country = $countries[$country];
    $sql = "UPDATE users SET salutation=%s, job_title=%s, first_name=%s, last_name=%s, email=%s, affiliation=%s, country=%s, city=%s, comment='Edited by admin' WHERE user_name=%s;";
    $params = array($salutation, $job_title, $first_name, $last_name, $email, $affiliation, $country, $city, $user_name);
    $params = array_map(array($db, 'quote'), $params);
    $sql = vsprintf($sql, $params);
    if (!$db->queryExec($sql, $error))
        die($error);
    show_site_users();
}

function edit_user() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $db = get_users_db();
    if (isset($_POST['user_name'])) {
        $user_name = $_POST['user_name'];
        $query = "SELECT * FROM users WHERE user_name=%s;";
        $query = sprintf($query, $db->quote($user_name));
    } elseif (isset($_POST['email'])) {
        $email = $_POST['email'];
        $query = "SELECT * FROM users WHERE email=%s;";
        $query = sprintf($query, $db->quote($email));
    }
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        $user = $result->fetch();
        unset($user['password']);
    } else {
        die($error);
    }
    $smarty->assign('salutations', get_salutation());
    $smarty->assign('job_titles', get_job_titles());
    $smarty->assign('countries', get_countries());
    $smarty->assign('post', get_profile_data($user));
    $smarty->display('edit_user.tpl');
}

function delete_user() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $user_name = $_POST['user_name'];
    $query = "SELECT email, assoc FROM users WHERE user_name = %s";
    $db = get_users_db();
    $query = sprintf($query, $db->quote($user_name));
    if (!$result = $db->query($query, SQLITE_ASSOC, $error))
        die($error);
    $row = $result->fetch();
    $assoc = $row['assoc'];
    $email = $row['email'];
    $db_file = FILES_DIR . "trial_${email}_${assoc}.sqlite";
    if (file_exists($db_file))
        unlink($db_file);
    $db = get_users_db();
    $sql = "DELETE FROM users WHERE user_name=%s;";
    $sql = sprintf($sql, $db->quote($user_name));
    if (!$db->queryExec($sql, $error))
        die($error);
    show_site_users();
}

function delete_request_email() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $email = $_POST['email'];
    $sql = "DELETE FROM users WHERE email=%s;";
    $db = get_users_db();
    $sql = sprintf($sql, $db->quote($email));
    if (!$db->queryExec($sql, $error))
        die($error);
}

function repair_user_db() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $db_file = $_POST['db_file'];
    if ($test_trial = new TestTrial($db_file))
        $test_trial->refresh_subjects_levels();
    show_site_users();
}

function toggle_user_status() {
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $user_name = $_POST['user_name'];
    $db = get_users_db();
    if ($_POST['action'] == 'enable user') {
        $msg_act = 'enabled';
        $sql = "UPDATE users SET disabled=0, disabled_reason=NULL WHERE user_name=%s;";
        $sql = sprintf($sql, $db->quote($user_name));
    } else {
        $msg_act = 'disabled';
        $sql = "UPDATE users SET disabled=1, disabled_reason=%s WHERE user_name=%s;";
        $params = array($_POST['disabled_reason'], $user_name);
        $params = array_map(array($db, 'quote'), $params);
        $sql = vsprintf($sql, $params);
    }
    $subject = "QMinim account {$msg_act}";
    $body = "Dear QMinim user\n\nYour account has been {$msg_act}!\n\n";
    $body .= "Qminim site admin\n\n";
    $body .= $_SESSION['userloggedin']['email'] . "\n\n";
    $body .= "If this email is not related to you please ignore it, or inform the site admin\n\n";
    if (!$db->queryExec($sql, $error))
        die($error);
    send_mail($subject, $body, $_POST['user_email']);
    show_site_users();
}

function show_admin_panel() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $smarty->display('admin_panel.tpl');
}

function show_site_users() {
    global $smarty;
    if (!Settings::isAdmin($_SESSION['userloggedin']['user_name']))
        header("Location: " . BASE_URL . 'index.php');
    $users = array();
    $query = "SELECT * FROM users WHERE user_name IS NOT NULL ORDER BY UPPER(user_name);";
    $db = get_users_db();
    $login_user_rank = Settings::getAdminRank($_SESSION['userloggedin']['user_name']);
    if ($login_user_rank > 1)
        $smarty->assign('can_toggle', true);
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        while($row = $result->fetch()) {
            unset($row['password']);
            $user = (array) $row;
            if ($row['disabled'])
                $user['status_img'] = 'disabled_img';
            else
                $user['status_img'] = 'enabled_img';
            $rank = Settings::getAdminRank($user['user_name']);
            if ($rank >= $login_user_rank) continue;
            if ($rank > 0) {
                $user['is_admin'] = true;
            }
            $users[] = $user;
        }
    } else {
        die($error);
    }
    $step_users = array();
    $query = "SELECT * FROM users WHERE user_name IS NULL ORDER BY UPPER(email);";
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        while($row = $result->fetch()) {
            $user = array();
            $user['email'] = $row['email'];
            if ($row['activation_code']) {
                $exp = explode(',', $row['activation_code']);
                if (count($exp) == 2) {
                    list($num_requests, $act_code) = $exp;
                    $user['num_requests'] = $num_requests;
                    $user['act_code'] = $act_code;
                } else {
                    $user['num_requests'] = '?' . $row['activation_code'];
                    $user['act_code'] = '?';
                }
            } else {
                    $user['num_requests'] = '';
                    $user['act_code'] = '';
            }
            $user['date_requested'] = $row['date_requested'];
            $step_users[] = $user;
        }
    } else {
        die($error);
    }
    if (isset($_POST['hash']))
        $smarty->assign('hash', $_POST['hash']);
    $smarty->assign('users', $users);
    $smarty->assign('step_users', $step_users);
    $smarty->display('site_users.tpl');
}

?>
