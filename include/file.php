<?php

/**
 * @file file.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function create_zip($files = array(), $destination = '', $overwrite = false) {
  //if the zip file already exists and overwrite is false, return false
  if(file_exists($destination) && !$overwrite) { return false; }
  //vars
  $valid_files = array();
  //if files were passed in...
  if(is_array($files)) {
    //cycle through each file
    foreach($files as $k => $file) {
      //make sure the file exists
      if(file_exists($file)) {
        $valid_files[$k] = $file;
      }
    }
  }
  //if we have good files...
  if(count($valid_files)) {
    //create the archive
    $zip = new ZipArchive();
    touch($destination);
    if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
      return false;
    }
    //add the files
    foreach($valid_files as $k => $file) {
      $zip->addFile($file,$k);
    }
    //debug
    //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
    
    //close the zip -- done!
    $zip->close();
    
    //check to make sure the file exists
    return file_exists($destination);
  }
  else
  {
    return false;
  }
}

function file_download($file, $name, $mime_type='') {
    /* The majority of this code was taken from:
     * http://w-shadow.com/blog/2007/08/12/how-to-force-file-download-with-php/
     * 
     * So a big thanks to them.
     * I have modified parts of it, though, so it's not 100% borrowed.
     */

    if(!is_readable($file)) die('File not found or inaccessible!');

    $size = filesize($file);
    $name = rawurldecode($name);

    /* Figure out the MIME type (if not specified) */

    if($mime_type==''){
        $known_mime_types = get_known_mime_types();
        $file_extension = strtolower(substr(strrchr($file,"."),1));

        if(array_key_exists($file_extension, $known_mime_types)){
            $mime_type=$known_mime_types[$file_extension];
        } else {
            $mime_type="application/force-download";
        }
    }

    @ob_end_clean(); //turn off output buffering to decrease cpu usage

    // required for IE, otherwise Content-Disposition may be ignored
    if(ini_get('zlib.output_compression')) {
        ini_set('zlib.output_compression', 'Off');
    }

    header('Content-Type: ' . $mime_type);
    header('Content-Disposition: attachment; filename="'.$name.'"');
    header("Content-Transfer-Encoding: binary");
    header('Accept-Ranges: bytes');

    /* The three lines below basically make the download non-cacheable */
    header("Cache-control: private");
    header('Pragma: private');
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

    // multipart-download and download resuming support
    if(isset($_SERVER['HTTP_RANGE'])) {
        list($a, $range) = explode("=",$_SERVER['HTTP_RANGE'],2);
        list($range) = explode(",",$range,2);
        list($range, $range_end) = explode("-", $range);
        $range=intval($range);

        if(!$range_end) {
            $range_end=$size-1;
        } else {
            $range_end=intval($range_end);
        }

        $new_length = $range_end-$range+1;

        header("HTTP/1.1 206 Partial Content");
        header("Content-Length: $new_length");
        header("Content-Range: bytes $range-$range_end/$size");
    } else {
        $new_length=$size;
        header("Content-Length: ".$size);
    }

    /* output the file itself */
    $chunksize = 1*(1024*1024); // 1MB, can be tweaked if needed
    $bytes_send = 0;

    if ($file = fopen($file, 'r')) {
        if(isset($_SERVER['HTTP_RANGE'])) {
            fseek($file, $range);
        }

        while(!feof($file) && (!connection_aborted()) && ($bytes_send<$new_length)) {
            $buffer = fread($file, $chunksize);
            print($buffer); //echo($buffer); // is also possible
            flush();
            $bytes_send += strlen($buffer);
        }

        fclose($file);
    } else {
        die('Error - can not open file.');
    }

    die();
}

function download_file($file) {
    /* this function is not used currently
     * use a db solution
     */ 
    if (file_exists($file)) {
        $type = "application/octet-stream";
        @ob_clean();
        header('Content-Description: File Transfer');
        header("Content-Type: $type");
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }
}

function show_download() {
    global $smarty;
    // copy the user sqlite file to a downloadable location
    $trial = get_trial_instance();
    $sha1 = sha1_file($trial->db_file);
    $md5 = md5_file($trial->db_file);
    $temp_dir  = CACHE_DIR . 'tmp';
    if (!file_exists($temp_dir))
        mkdir($temp_dir, 0777, true);
    $tmpsha1 = tempnam($temp_dir, 'trial_sha1');
    file_put_contents($tmpsha1, $sha1);
    $tmpmd5 = tempnam($temp_dir, 'trial_md5');
    file_put_contents($tmpmd5, $md5);
    $files_to_zip = array('my-trial.sqlite' => $trial->db_file, 'sha1.txt' => $tmpsha1, 'md5.txt' => $tmpmd5);
    $user_name = $_SESSION['userloggedin']['user_name'];
    if ($result = create_zip($files_to_zip, "$temp_dir/my-trial-{$user_name}.zip", true)) {
	    $smarty->assign('sha1checksum', sha1_file("$temp_dir/my-trial-{$user_name}.zip"));
	    $smarty->assign('md5checksum', md5_file("$temp_dir/my-trial-{$user_name}.zip"));
	    $smarty->display("download.tpl");
    } else {
        $smarty->display("download_error.tpl");
    }
}

function download_trial($download=True) {
     // redirect the user to and intermediate download page
    $user_name = $_SESSION['userloggedin']['user_name'];
    $temp_dir  = CACHE_DIR . 'tmp';
    if (!file_exists($temp_dir))
        mkdir($temp_dir, 0777, true);
    if (file_exists("$temp_dir/my-trial-{$user_name}.zip")) {
        file_download("$temp_dir/my-trial-{$user_name}.zip", "my-trial-{$user_name}.zip", "application/zip");
    } else {
        $smarty->display("download_error.tpl");
    }
}

function checksum_uploaded_file() {
    $f = $_FILES['uploadedtrialfile']['tmp_name'];
    $sha1 = $_POST['sha1'];
    $md5 = $_POST['md5'];
    if ($sha1) {
        if ($sha1 != sha1_file($f))
            die('Invalid uploaded file!');
    }
    if ($md5) {
        if ($md5 != md5_file($f))
            die('Invalid uploaded file!');
    }
}

function upload_zip_file() {
    global $smarty;
    $test_trial_folder = null;
    $save_msg = array();
    $max_trial_file_size =& Settings::getSetting('max_trial_file_size');
    if ($_FILES['uploadedtrialfile']['size'] > $max_trial_file_size) {
        $save_msg[] = "The trial file must be less than " . $max_trial_file_size/1024 . " KB!";
    } else {
        $temp_dir  = CACHE_DIR . 'tmp';
        if (!file_exists($temp_dir))
            mkdir($temp_dir, 0777, true);
        $temp_trial_dir = tempnam($temp_dir, 'trial_');
        # we don't wnat a file, but a folder
        unlink($temp_trial_dir);
        mkdir($temp_trial_dir);
        $zip = new ZipArchive();
        if ($zip->open($_FILES['uploadedtrialfile']['tmp_name']) === TRUE) {
            if ($zip->extractTo($temp_trial_dir)) {
                $files = array('my-trial.sqlite', 'sha1.txt', 'md5.txt');
                foreach ($files as $f) {
                    if (!file_exists($temp_trial_dir . '/' . $f)) {
                        $save_msg[] = "Invalid trial zip file!";
                    } else {
                        if (filesize($temp_trial_dir . '/' . $f) > $max_trial_file_size) {
                            $save_msg[] = "The trial file must be less than " . $max_trial_file_size/1024 . " KB!";
                        }
                    }
                }
            } else {
                $save_msg[] = "Invalid trial zip file!";
            }
            if (!$save_msg) {
                $sha1 = file_get_contents($temp_trial_dir . '/sha1.txt');
                if ($sha1) {
                    if ($sha1 != sha1_file($temp_trial_dir . '/my-trial.sqlite'))
                        die('Invalid uploaded file!');
                }
                $md5 = file_get_contents($temp_trial_dir . '/md5.txt');
                if ($md5) {
                    if ($md5 != md5_file($temp_trial_dir . '/my-trial.sqlite'))
                        die('Invalid uploaded file!');
                }
            }
        } else {
            $save_msg[] = "Invalid trial zip file!";
        }
        if (!$save_msg) {
            $test_trial_folder = $temp_trial_dir;
            unlink($temp_trial_dir . '/sha1.txt');
            unlink($temp_trial_dir . '/md5.txt');
        }
    }
    return array($save_msg, $test_trial_folder);
}

function trial_uploaded() {
    global $smarty;
    if (isset($_FILES['uploadedtrialfile']) && $_FILES['uploadedtrialfile']['size']) {
        if (($_FILES['uploadedtrialfile']['type'] != 'application/zip') && ($_FILES['uploadedtrialfile']['type'] != 'application/x-zip-compressed')) {
            $save_msg = array("Only zip files accepted!");
        } else {
            list($save_msg, $test_trial_folder) = upload_zip_file();
            if (DB::check_db_file($test_trial_folder . '/my-trial.sqlite')) {
                try {
                    if (!$test_trial = new TestTrial($test_trial_folder . '/my-trial.sqlite')) {
                        $save_msg[] = "Invalid trial file!";
                    } else {
                        if (!$test_trial->inited) {
                            $save_msg[] = $test_trial->error;
                        } else {
                            if (!$summary = $test_trial->get_summary())
                                $save_msg[] = 'Invalid trial file';
                        }
                    }
                } catch (Exception $e) {
                    $save_msg[] = "Invalid trial file!";
                }
            } else {
                    $save_msg[] = "Invalid sqlite db file!";
            }
        }
    } else {
        $save_msg = array("Please select your trial file!");
    }
    if ($save_msg) {
        $smarty->assign('save_msg', implode(', ', array_unique($save_msg)));
        $max_trial_file_size = Settings::getSetting('max_trial_file_size');
        $smarty->assign('max_file_size', $max_trial_file_size);
        $smarty->display('upload_trial.tpl');
    } else {
        $test_trial->refresh_subjects_levels();
        $smarty->assign('test_summary', $summary);
        $smarty->assign('test_trial_folder', $test_trial_folder);
        $smarty->assign('test_treatments', $test_trial->treatments);
        $smarty->assign('test_factors', $test_trial->factors);
        $smarty->assign('test_subjects', $test_trial->subjects);
        $smarty->assign("test_frequencies", $test_trial->frequencies);
        $smarty->assign("test_pgrand_total", $test_trial->get_grand_total($test_trial->preload));
        $smarty->assign("test_fgrand_total", $test_trial->get_grand_total($test_trial->frequencies));
        $smarty->assign("test_preload", $test_trial->preload);

        $smarty->assign("test_prob_method", $test_trial->prob_method);
        $smarty->assign("test_dist_method", $test_trial->dist_method);
        $smarty->assign("test_base_prob", $test_trial->base_prob);

        $smarty->assign("test_sample_size", $test_trial->sample_size);
        $smarty->assign("test_id_type", $test_trial->id_type);
        $smarty->assign("test_id_order", $test_trial->id_order);

        $smarty->assign("test_status", $test_trial->status);
        $smarty->assign("test_date_initiated", $test_trial->date_initiated);

        display_index_page('test_trial.tpl');
    }
}

function apply_trial_upload() {
    $trial = get_trial_instance();
    $new_trial_folder = $_POST['test_trial_folder'];
    $new_trial_file = $new_trial_folder . '/my-trial.sqlite';
    copy($new_trial_file, $trial->db_file);
    unlink($new_trial_file);
    rmdir($new_trial_folder);
    unset($_SESSION['subjectsfilter']);
    header("Location: " . BASE_URL . 'index.php');
}
?>
