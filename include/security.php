<?php

/**
 * @file security.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function check_csrf() {
    global $smarty;
    if (count($_POST)) {
        if (!isset($_POST['CSRFName'])) {
            $subpage = 'process_error.tpl';
            session_unset();
        } else {
            $name =$_POST['CSRFName'];
            $token=$_POST['CSRFToken'];
            if (!csrfguard_validate_token($name, $token)) {
                $subpage = 'process_error.tpl';
                session_unset();
            }
        }
    }
    common_smarty_assign();
    if (isset($subpage) && $subpage == 'process_error.tpl') {
        $smarty->assign('subpage', $subpage);
        $smarty->display('index.tpl');
        exit();
    }
}

function csrfguard_generate_token($unique_form_name) {
    if (function_exists("hash_algos") and in_array("sha512",hash_algos())) {
        $token=hash("sha512",mt_rand(0,mt_getrandmax()));
    } else {
        $token='';
        for ($i=0;$i<128;++$i)    {
            $r=mt_rand(0,35);
            if ($r<26) {
                $c=chr(ord('a')+$r);
            } else { 
                $c=chr(ord('0')+$r-26);
            } 
            $token.=$c;
        }
    }
    store_in_session($unique_form_name,$token);
    return $token;
}

function csrfguard_validate_token($unique_form_name, $token_value) {
    $token = get_from_session($unique_form_name);
    if ($token===false)     {
        $result=true;
    } elseif ($token==$token_value) {
        $result=true;
    } else { 
        $result=false;
    }
    unset_session($unique_form_name);
    return $result;
}

function store_in_session($key,$value) {
    if (isset($_SESSION)) {
        $_SESSION[$key]=$value;
    }
}

function unset_csrf_vars() {
    $ks = array();
    foreach($_SESSION as $k => $v) {
        if (strstr($k, 'CSRFGuard')) {
            $ks[] = $k;
        }
    }
    foreach($ks as $k) {
        $_SESSION[$k]='';
        unset($_SESSION[$k]);
    }
}

function unset_session($key) {
    $_SESSION[$key]='';
    unset($_SESSION[$key]);
    $ks = array();
    foreach($_SESSION as $k => $v) {
        if (strstr($k, 'CSRFGuard')) {
            $ks[] = $k;
        }
    }
    foreach($ks as $k) {
        $_SESSION[$k]='';
        unset($_SESSION[$k]);
    }
}

function get_from_session($key) {
    if (isset($_SESSION)) {
        if (isset($_SESSION[$key]))
            return @$_SESSION[$key];
        else
            return 'no_token';
    } else {
        return false;
    } //no session data, no CSRF risk
}

function get_csrf($params, &$smarty) {
    $name = "CSRFGuard_" . mt_rand(0, mt_getrandmax());
    $token = csrfguard_generate_token($name);
    $ret = "<input type='hidden' name='CSRFName' value='{$name}' />";
    $ret .= "<input type='hidden' name='CSRFToken' value='{$token}' />";
    return $ret;
}

function validate_post($req) {
    $err_msg = array();
    foreach ($req as $item) {
        if (get_user_var($item, 'post_not_provided') == 'post_not_provided')
            $err_msg[] = titled($item) . " is requited!";
    }
    if (isset($_POST['email'])) {
        if (!is_valid_email($_POST['email']) or bad_newline($_POST['email'])) {
            $err_msg[] = "Email address is not valid!";
        }
    }
    return $err_msg;
}

function my_hasher($info, $encdata=false) {
    $cost = '08';
    if ($encdata) {
        if (substr($encdata, 0, 60) == crypt($info, "$2a$".$strength."$".substr($encdata, 60))) {
            return true;
        } else {
            return false;
        }
    } else {
        $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
        $salt = sprintf('$2a$%02d$', $cost) . $salt;
        return crypt($info, $salt).$salt; 
    }
}

function hasher($pw, $encdata=false) {   
    require_once(BASE_DIR . 'include/PasswordHash.php');
    $t_hasher = new PasswordHash(8, FALSE);
    if ($encdata) {
        $hash_type = Settings::getSetting('hash_type_id');
        if (substr($encdata, 0, strlen($hash_type)) != $hash_type) {
            return sha1($pw) == $encdata;
        }
        return $t_hasher->CheckPassword($pw, $encdata) == 1;
    } else {
       return $t_hasher->HashPassword($pw);
    }
}

function check_captcha() {
    if (! isset($_POST['captcha_code'])) return true;
    $securimage = new Securimage();
    return $securimage->check($_POST['captcha_code']);
}

function bad_newline($str_to_test) {
    if (contains_bad_str($str_to_test))
        return true;
    if (contains_newlines($str_to_test))
        return true;
    return false;
}

function contains_bad_str($str_to_test) {
    $bad_strings = array(
                         "content-type:",
                         "mime-version:",
                         "multipart/mixed",
                         "Content-Transfer-Encoding:",
                         "bcc:",
                         "cc:",
                         "to:"
    );
  
    foreach($bad_strings as $bad_string) {
        if(strstr($bad_string, strtolower($str_to_test))) {
            return true;
        }
    }
    return false;
}

function contains_newlines($str_to_test) {
    if(preg_match("/(%0A|%0D|\\n+|\\r+)/i", $str_to_test) != 0) {
        return true;
    }
    return false;
}

?>
