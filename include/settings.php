<?php

/**
 * @file settings.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 


class Settings {

    var $admins = array();
    var $settings = array();
    var $fields = array();
    var $topadmin;
    
    function Settings() {
        $this->readSettings();
    }

    function readSettings() {
        $this->admins = array();
        $this->settings = array();
        $this->fields = array();
        $query = "SELECT * FROM settings ORDER BY UPPER(setting_name);";
        $db =& DB::getSettingsDB();
        if($result = $db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $this->fields[$row['setting_name']] = $row['setting_type'];
                $this->settings[$row['setting_name']] = $row['setting_value'];
            }
        } else {
            die($error);
        }

        $this->admins = array();
        $query = "SELECT * FROM admins ORDER BY rank DESC;";
        $db =& DB::getSettingsDB();
        if($result = $db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $this->admins[$row['user_name']] = $row['rank'];
            }
        } else {
            die($error);
        }

        $query = "select * from admins where rank = (select max(rank) from admins);";
        $db =& DB::getSettingsDB();
        if($result = $db->query($query, SQLITE_ASSOC, $error)) {
            $row = $result->fetch();
            $query = "select * from users where user_name = %s;";
            $db =& DB::getUserDB();
            $query = sprintf($query, $db->quote($row['user_name']));
            if($result = $db->query($query, SQLITE_ASSOC, $error)) {
                $row = $result->fetch();
                $this->topadmin = (array) $row;
            } else {
                die($error);
            }
        } else {
            die($error);
        }
    }

    public static function &getSetting($name, $default=null) {
        $instance = Settings::getInstance();
        if (array_key_exists($name, $instance->settings))
            return $instance->settings[$name];
        else
            return $default;
    }
    
    public static function &getSettings() {
        $instance = Settings::getInstance();
        $ret = array();
        foreach($instance->settings as $name => $value) {
            $ret[$name] = array('value' => $value, 'type' => $instance->fields[$name]);
        }
        return $ret;
    }

    public static function addSetting($name, $value, $type) {
        $sql = "INSERT INTO settings VALUES (###)";
        $db =& DB::getSettingsDB();
        $params = array($name, $value, $type);
        $sql = $db->replace_params($sql, $params);
        if(!$db->queryExec($sql, $error))
            die($error);
        $instance = Settings::getInstance();
        $instance->readSettings();
    }

    public static function updateSetting($old_name, $new_name, $value, $type) {
        $sql = "UPDATE settings SET setting_name = %s, setting_value = %s, setting_type = %s WHERE setting_name = %s;";
        $db =& DB::getSettingsDB();
        $params = array($new_name, $value, $type, $old_name);
        $params = array_map(array($db, 'quote'), $params);
        $sql = vsprintf($sql, $params);
        if(!$db->queryExec($sql, $error))
            die($error);
        $instance = Settings::getInstance();
        $instance->readSettings();
    }

    public static function updateSettings($data) {
        $sql = array();
        foreach($data as $field => $value) {
            $sql[] = "SET {$field} = {$sep}{$value}{$sep}";
        }
        $set = implode(', ', $sql);
        $sql = "UPDATE settings SET {$set};";
        $db =& DB::getSettingsDB();
        if(!$this->db->queryExec($query, $error))
            die($error);
        $instance = Settings::getInstance();
        foreach($data as $field => $value) {
            $instance->settings[$field] = $value;
        }
    }

    public static function deleteSetting($name) {
        $sql = "DELETE FROM settings WHERE setting_name = %s;";
        $db =& DB::getSettingsDB();
        $sql = sprintf($sql, $db->quote($name));
        if(!$db->queryExec($sql, $error))
            die($error);
        $instance = Settings::getInstance();
        unset($instance->settings[$name]);
        unset($instance->fields[$name]);
    }

    public static function updateSettingName($old_name, $new_name) {
        $sql = "UPDATE settings SET setting_name = %s WHERE setting_name = %s;";
        $db =& DB::getSettingsDB();
        $params = array($new_name, $old_name);
        $params = array_map(array($db, 'quote'), $params);
        $sql = vsprintf($sql, $params);
        if(!$db->queryExec($sql, $error))
            die($error);
        $instance = Settings::getInstance();
        $instance->readSettings();
    }

    public static function updateSettingValue($name, $value) {
        $sql = "UPDATE settings SET setting_value = %s WHERE setting_name = %s;";
        $db =& DB::getSettingsDB();
        $params = array($value, $name);
        $params = array_map(array($db, 'quote'), $params);
        $sql = vsprintf($sql, $params);
        if(!$db->queryExec($sql, $error))
            die($error);
        $instance = Settings::getInstance();
        $instance->settings[$name] = $value;
    }

    public static function updateSettingType($name, $type) {
        $sql = "UPDATE settings SET setting_type = %s WHERE setting_name = %s;";
        $db =& DB::getSettingsDB();
        $params = array($type, $name);
        $params = array_map(array($db, 'quote'), $params);
        $sql = vsprintf($sql, $params);
        if(!$db->queryExec($sql, $error))
            die($error);
        $instance = Settings::getInstance();
        $instance->fields[$name] = $type;
    }

    public static function isAdmin($user_name) {
        $instance = Settings::getInstance();
        return in_array($user_name, array_keys($instance->admins));
    }

    public static function &getInstance() {
        static $instance;
        if (!isset($instance)) {
            $instance = new Settings();
        }
        return $instance;
    }

    public static function &getAdmins() {
        $instance = Settings::getInstance();
        return $instance->admins;
    }

    public static function &getTopAdmin() {
        $instance = Settings::getInstance();
        return $instance->topadmin;
    }
    
    public static function adminToggle($user_name) {
        $instance = Settings::getInstance();
        if (in_array($user_name, array_keys($instance->admins))) {
            $sql = "DELETE FROM admins WHERE user_name = %s;";
            $db =& DB::getSettingsDB();
            $sql = sprintf($sql, $db->quote($user_name));
            if(!$db->queryExec($sql, $error))
                die($error);
            unset($instance->admins[$user_name]);
        } else {
            $sql = "INSERT INTO admins VALUES (###);";
            $db =& DB::getSettingsDB();
            $params = array($user_name, 1);
            $sql = $db->replace_params($sql, $params);
            if(!$db->queryExec($sql, $error))
                die($error);
            $instance->admins[$user_name] = 1;
        }
    }

    public static function getAdminRank($user_name) {
        $instance = Settings::getInstance();
        if (!in_array($user_name, array_keys($instance->admins)))
            return 0;
        return $instance->admins[$user_name];
    }
}

?>
