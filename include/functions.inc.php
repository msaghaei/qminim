<?php

/**
 * @file functions.inc.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit();

function get_trial_instance($refresh=false) {
    if (!isset($_SESSION['userloggedin']))
        return;
    $trial_obj =& Trial::getTrial($_SESSION['userloggedin'], $refresh);
    return $trial_obj;
}

function get_users_db() {
    $db =& DB::getUserDB();
    return $db;
}

function reset_trial() {
    /* copy a sample new trial file to the FILES_DIR and name the file
     * <username>.sqlite. FILES_DIR . '<username>.sqlite'
     */
    $trial = get_trial_instance();
    if (file_exists($trial->db_file))
        unlink($trial->db_file);
    unset($_SESSION['subjectsfilter']);
    header("Location: " . BASE_URL . 'index.php');
}

function get_user_var($var_name, $default = null) {
    static $request;
    if (!isset($request))
        $request = $_POST;
    if (!isset($request[$var_name]))
        $request[$var_name] = $default;
    return strip_custom($request[$var_name]);
}

function get_page_indices($numrows) {
    $totalpages = ceil($numrows / Settings::getSetting('rows_per_page'));
    if (isset($_POST['currentpage']) && is_numeric($_POST['currentpage'])) {
        $currentpage = (int) $_POST['currentpage'];
    } else {
        if (isset($_POST['currentpage'])) {
            switch($_POST['currentpage']) {
                case 'last':
                    $currentpage = $totalpages;
                    break;
                case 'first':
                    $currentpage = 1;
                    break;
                default:
                    $currentpage = 1;
            }
        } else {
            $currentpage = 1;
        }
    }
    if ($currentpage > $totalpages) {
        $currentpage = $totalpages;
    }
    if ($currentpage < 1) {
        $currentpage = 1;
    }
    $offset = ($currentpage - 1) * Settings::getSetting('rows_per_page');
    return array($currentpage, $offset);
}

function display_index_page($subpage=null, $get_subpage=false) {
    global $smarty;
    $trial = get_trial_instance();
    if (isset($_POST['subpage']))
        $subpage = $_POST['subpage'];
    if ($subpage == 'settings.tpl') {
        if (!$smarty->get_template_vars('new_sample_size')) {
            $new_sample_size = $trial->sample_size + 10;
            if ($new_sample_size > Settings::getSetting('max_allowed_subjects'))
                $new_sample_size = Settings::getSetting('max_allowed_subjects');
            $smarty->assign('new_sample_size', $new_sample_size);
        }
    }
    if (!$subpage) {
        if ($trial->status == 'Enroll' or $trial->status == 'Finished')
            $subpage = "subjects.tpl";
        else
            $subpage = "trials.tpl";
    }
    if ($subpage == 'preload.tpl') {
        if (!isset($_POST['preloadtype'])) {
            $smarty->assign("preloadtype", 'preloaddr.tpl');
        } else {
            $smarty->assign("preloadtype", $_POST['preloadtype']);
        }
    }
    if ($subpage == 'frequencies.tpl') {
        if (!isset($_POST['frequenciestype'])) {
            $smarty->assign("frequenciestype", 'frequenciesdr.tpl');
        } else {
            $smarty->assign("frequenciestype", $_POST['frequenciestype']);
        }
    }
    $smarty->assign("user", $_SESSION['userloggedin']);
    $smarty->assign('subpage', $subpage);

    list($trials_info, $cur_trial, $new_trial_file) = get_trials_info($_SESSION['userloggedin']);
    $max_allowed_trials = Settings::getSetting('max_allowed_trials', 1);
    $smarty->assign("trials", $trials_info);
    $smarty->assign("cur_trial", $cur_trial);
    $smarty->assign("max_allowed_trials", $max_allowed_trials);
    $smarty->assign("new_trial_file", $new_trial_file);
    $smarty->assign("treatments", $trial->treatments);
    $smarty->assign("factors", $trial->factors);
    $smarty->assign("title", $trial->title);
    $smarty->assign("prob_method", $trial->prob_method);
    $smarty->assign("dist_method", $trial->dist_method);
    $smarty->assign("base_prob", $trial->base_prob);

    $smarty->assign("sample_size", $trial->sample_size);
    $smarty->assign("id_type", $trial->id_type);
    $smarty->assign("id_order", $trial->id_order);
    $smarty->assign("recycle_ids", $trial->recycle_ids);

    $smarty->assign("status", $trial->status);
    $smarty->assign("date_initiated", $trial->date_initiated);
    $smarty->assign("date_modified", date('Y-m-d H:i:s', filemtime($trial->db_file)));

    $smarty->assign("preload", $trial->preload);
    $smarty->assign("preloadtp", $trial->get_preload_transpose());

    if (! isset($_SESSION['subjectsfilter']))
        set_subjects_filter();
    $selected_treatments = $_SESSION['subjectsfilter']['selected_treatments'];
    $sel_treatments = $_SESSION['subjectsfilter']['sel_treatments'];
    $selected_factors = $_SESSION['subjectsfilter']['selected_factors'];
    $sel_factors = $_SESSION['subjectsfilter']['sel_factors'];
    $selected_id = $_SESSION['subjectsfilter']['selected_id'];

    $numrows = $trial->get_num_selected_subjects($sel_treatments, $sel_factors, $selected_id);
    list($currentpage, $offset) = get_page_indices($numrows);

    $smarty->assign("selected_treatments", $selected_treatments);
    $smarty->assign("selected_factors", $selected_factors);
    $smarty->assign("selected_id", $selected_id);

    $selected_subjects = $trial->get_subjects_limited($offset, Settings::getSetting('rows_per_page'), $sel_treatments, $sel_factors, $selected_id);
    if ($subpage == 'trial_summary.tpl') {
        $smarty->assign("subjects", $trial->subjects);
    } else {
        $smarty->assign("subjects", $selected_subjects);
    }
    $smarty->assign("all_subjects_count", count($trial->subjects));

    $smarty->assign("numrows", $numrows);
    $smarty->assign("currentpage", $currentpage);

    $smarty->assign("frequencies", $trial->get_frequencies());
    $smarty->assign("frequenciestp", $trial->get_frequencies_transpose());
    $smarty->assign("balance", $trial->balance);
    $pgt = $trial->get_grand_total($trial->preload);
    $smarty->assign("pgrand_total", $pgt);
    $fgt = $trial->get_grand_total($trial->frequencies);
    $ip = $_SESSION['subjectsfilter']['include_preload'];
    if (!$ip) {
        foreach($fgt as $k => $v) {
            $fgt[$k] -= $pgt[$k];
        }
    }
    $smarty->assign("include_preload", $ip);
    $smarty->assign("fgrand_total", $fgt);
    unset_csrf_vars();
    if ($get_subpage) {
        return $smarty->fetch($subpage);
    } else {
        $smarty->display('index.tpl');
    }
}

function get_trials_info($user) {
    $trial = get_trial_instance();
    $indices = array();
    $ret = array();
    $email = $user['email'];
    $assoc = $user['assoc'];
    $n = 0;
    foreach (glob(FILES_DIR . "trial_${email}_${assoc}*.sqlite") as $db_file) {
        if ($trial->db_file == $db_file) {
            $cur_trial = $n;
        }
        $n++;
        $i = str_replace(FILES_DIR . "trial_${email}_${assoc}", '', $db_file);
        $i = str_replace('.sqlite', '', $i);
        $i = str_replace('_', '', $i);
        if ($i == '')
            $i = '0';
        $i = (int) $i;
        $indices[] = $i;
        $db = new DB($db_file);
        $query = 'SELECT * FROM settings;';
        if($result = $db->query($query, SQLITE_ASSOC, $error)) {
            $row = $result->fetch();
            if (array_key_exists('title', $row)) {
                $title = $row['title'];
            } else {
                $title = '';
            }
            $f = str_replace(FILES_DIR, '', $db_file);
            $ret[$f] = $title;
        }  else {
            die($error);
        }
    }
    $i = max($indices) + 1;
    foreach(range(0, $i) as $n) {
        if (!in_array($n, $indices))
            break;
    }
    if ($n == 0)
        $n = '';
    else
        $n = "_{$n}";
    $new_file = "trial_${email}_${assoc}{$n}.sqlite";
    return array($ret, $cur_trial, $new_file);
}

function get_salutation() {
    return array(
                   "" => "",
                   "Dr" => "Dr",
                   "Prof" => "Prof",
                   "Mr" => "Mr",
                   "Ms" => "Ms",
                   "Miss" => "Miss",
                   "Mrs" => "Mrs");
}

function get_job_titles() {
    $s = trim(file_get_contents(BASE_DIR . 'job_titles.dat'));
    $job_titles = array("" => "");
    $s = explode("\n", $s);
    return array_merge($job_titles, $s);
}

function get_countries() {
    $s = file_get_contents(BASE_DIR . 'countries.dat');
    $countries = array("" => "");
    $s = explode("\n", $s);
    foreach($s as $line) {
        $expl = explode(":", $line);
        if ($line)
            $countries[$expl[1]] = $expl[0];
    }
    return $countries;
}

function get_supporttypes() {
    return array(
                   "Question" => "Question",
                   "Help" => "Help",
                   "Bug" => "Bug",
                   "Comment" => "Comment",
                   "Propose" => "Propose",
                   "Other" => "Other");
}

function manage_support_data () {
    global $smarty;
    $err_msg = validate_post(array('supporttype', 'subject_text', 'comment'));
    $ip = $_SERVER['REMOTE_ADDR'];
    $body = "$ip";
    foreach ($_POST as $key => $item) {
        $body .= "{$item}";
    }
    if (strip_custom($body) != $body or contains_bad_str($body))
        $err_msg[] = "Bad input!";
    if ($err_msg) {
        $smarty->assign('post', $_POST);
	    $smarty->assign('supporttypes', get_supporttypes());
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        $smarty->display('support.tpl');
    } else {
        $admin  = Settings::getTopAdmin();
        $to = $admin['email'];
        send_mail($_POST['subject_text'], $_POST['comment'], $to);
        $smarty->display('support_sent.tpl');
    }
}

function get_eula() {
	    $eula = file_get_contents(BASE_DIR . 'eula.txt');
	    $max_trials = Settings::getSetting('max_allowed_trials', 1);
	    if ($max_trials == '0')
	        $max_trials = 'Number of concurrent trials (dataset) is Unlimited';
        else
	        $max_trials = sprintf('Maximum number of concurrent trials (dataset) is limited to %s trial(s)', $max_trials);
	    $eula = sprintf($eula, 
	                    Settings::getSetting('max_allowed_subjects'),
	                    Settings::getSetting('max_allowed_treatments'),
	                    Settings::getSetting('rows_per_page'),
	                    Settings::getSetting('max_allowed_levels'),
	                    Settings::getSetting('max_allowed_chars'),
	                    $max_trials);
        return $eula;
}

?>
