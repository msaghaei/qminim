{*
 * registered.tpl
 *}
{include file='header.tpl'}
<h3 class="warning">Thank you for registration. {$registered_msg}</h3>
<p align="center">
{submit_link action=login_page label=Login}
</p>
{include file='footer.tpl'}
