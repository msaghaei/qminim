<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th rowspan='2' align='center' valign='middle'>
{$preload_tt|tts:""}
{submit_link action=editpreload preloadtype="preloaddr.tpl" src=$edit_img}
{submit_link action=clearpreload preloadtype="preloaddr.tpl" src=$clear_img confirm_msg="Clear preload?"}
{submit_link action=index_page subpage="preload.tpl" preloadtype="preloadtp.tpl" alt=transpose src=$transposetp_img}
</th>
{foreach from=$factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {array_count|assign:cs arr=$levels}
    <th colspan='{$cs}' align='center'>{$factor}</th>
{/foreach}
<th rowspan='2' align='center' valign='middle'>Total</th>
</tr><tr>
{foreach from=$factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {foreach from=$levels item=level}
        <th align='center'>{$level}</th>
    {/foreach}
{/foreach}
</tr>
{foreach from=$preload key=treatment item=factors}
    <tr>
    <th align='left'>{$treatment}</th>
    {foreach from=$factors key=factor item=levels}
        {arr_sum|assign:s arr=$levels}
        {foreach from=$levels key=level_name item=level_value}
            <td align='center'>{$level_value}</td>
        {/foreach}
    {/foreach}
    <td align='center'>{$s}</td>
    </tr>
{/foreach}
<tr><th align='left'>Total</th>
{foreach from=$pgrand_total item=v}
    <td align='center'>{$v}</td>
{/foreach}
</tr></table>
