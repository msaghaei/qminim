{*
 * treatments.tpl
 *}
<table class="listing">
<tr class="heading">
<td colspan="2" width="40px" align="center">
{if $status == 'Setup' && count($treatments) < $max_allowed_treatments}
{submit_link action=addtreatment src=$add_img}
{else}
{$addd_img}
{/if}
{$treatments_tt|tts}
</td><td>Treatment</td><td width="40px">Ratio</td>
</tr>
{foreach from=$treatments key=treatment item=ratio name=forloop}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td align="center" class="action">
    {if count($subjects)}
        {submit_link action=edittreatment treatment=$treatment initialtreatment=$treatment src=$edit_img confirm_msg="Changing alocation ratios may invalidate the minimisation. Some subjects already enrolled! Are you sure?"}
    {else}
        {submit_link action=edittreatment treatment=$treatment initialtreatment=$treatment src=$edit_img}
    {/if}
    </td>
    <td align="center" class="action">
    {if $status == 'Setup' and count($treatments) > 2}
        {submit_link action=deletetreatment treatment=$treatment src=$del_img}
    {else}
        {$deld_img}
    {/if}
    </td>
    <td>{$treatment}</td><td>{$ratio}</td></tr>
{/foreach}
</table>
