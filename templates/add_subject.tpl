{*
 * add_subject.tpl
 *}
 
{include file='header.tpl'}
<form action='index.php' method='post'>
{csrf}
<input type="hidden" name="action" value="subjectadded" />
<input type="hidden" name="currentpage" value="last" />
<div align="center">
<h2>Add Subject</h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<table>
{foreach from=$factors key=factor item=v}
    <tr><th align="right">{$factor}: </th><td>{html_options name=$factor options=$options_arr[$factor] selected=$selects_arr[$factor]}</td></tr>
{/foreach}
</table>
<input type="submit" name="submit" value="Add" class="defaultButton" />&nbsp;&nbsp;
{submit_link action=index_page label=Return subpage="subjects.tpl" tag=button}
</div>
</form>
{include file='footer.tpl'}
