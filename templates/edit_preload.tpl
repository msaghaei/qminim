{*
 * edit_preload.tpl
 *}
{include file='header.tpl'}
<h2>Edit Preload</h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<form action='index.php' method='post' onsubmit="return valid_preload_form(this);">
{csrf}
<input type="hidden" name="action" value="preloadedited" />
<input type="hidden" name="preloadtype" value="{$preloadtype}" />
{include file=edit_$preloadtype}
<br />
<input type="submit" name="submit" id="submit_preload_id" value="Save" class="defaultButton" />&nbsp;&nbsp;
{submit_link action=index_page label=Return subpage="preload.tpl" preloadtype="$preloadtype"}
</form>
{include file='footer.tpl'}
