{*
 * pre_register.tpl
 *}
{include file='header.tpl'}
{if isset($err_msg)}
    {$err_msg}
{/if}
<form action="index.php" method="post" name="pre_register_form" onsubmit="return valid_pre_register_form();">
{csrf}
<h3>Email Activation Form</h3>
<div class="warning">Please activate your email. You will recieved an activation code.</div>
<input type="hidden" name="action" value="pre_register_data">
<div align="center" class="register">
<table>
	<tr>
		<td><b>Email:</b>{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="email" id="email_id" value="{if isset($post)}{$post.email}{/if}" size="39" />
		</td>
	</tr>
	<tr>
	    <td>If you have already received an activation code by email, please enter in the box below to continue your registration!<br />
	    <b>Activation Code:</b><br />
		<input onChange="trim_value(this);" type="text" name="activation_code" id="activation_code_id" value="{if isset($post)}{if isset($post.activation_code)}{$post.activation_code}{/if}{/if}" size="39" />
		</td>
	</tr>
	<tr><td><br /></td></tr>
    <tr>
		<td>{$captcha_img}<br />
		{$astrisk}<b>Enter the above characters</b><br />
	    <input onChange="trim_value(this);" type="text" name="captcha_code" id="captcha_code_id" size="21" maxlength="6" />
        <a href="#" onclick="document.getElementById('captcha').src = '{$base_url}lib/securimage/securimage_show.php?' + Math.random(); return false">{$refresh_img}</a>
		</td>
	</tr>
	<tr>
		<td><input type="submit" id="pre_register_id" value="Submit" />
	    <input type="reset" />
		</td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
