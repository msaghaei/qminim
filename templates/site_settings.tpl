{*
 * site_settings.tpl
 *}
{include file='header.tpl'}
{include file='admin_menu.tpl'}
<h3>Site settings</h3>
<font color="red">Changing these values may severely compromises the function of the site. Please be careful!</font>
<table class="listing">
<tr valign='top' class="heading">
<td colspan='2' width='30px' align='center'>
{submit_link action=addsitesetting src=$add_img prompt_msg="new_setting:Enter new setting (name/value/type):----/----/----"}
</td>
<td>Setting name</td>
<td>Setting value</td>
<td>Setting type</td>
</tr>
{foreach from=$settings key=name item=setting name=forloop}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td align='center'>
    {capture name=snvt assign=settingnvt}{$name}/{$setting.value}/{$setting.type}{/capture}
    {submit_link action=editsitesetting setting_name=$name src=$edit_img prompt_msg="new_setting:Enter new setting (name/value/type):$settingnvt"}
    </td>
    <td align='center'>
    {submit_link action=deletesitesetting setting_name=$name src=$del_img confirm_msg="Are you sure you want to delete this setting?"}
    </td>
    <td>
    {submit_link action=changesettingname setting_name=$name label=$name tag=a same_case=yes prompt_msg="new_setting_name:Enter new setting name:$name"}
    </td>
    <td>
    {assign var='settingvalue' value=$setting.value}
    {submit_link action=changesettingvalue setting_name=$name label=$setting.value tag=a same_case=yes prompt_msg="new_setting_value:Enter new setting value:$settingvalue"}
    </td>
    <td>
    {assign var='settingtype' value=$setting.type}
    {submit_link action=changesettingtype setting_name=$name label=$setting.type tag=a same_case=yes prompt_msg="new_setting_type:Enter new setting type (String / Integer):$settingtype"}
    </td>
    </tr>
{/foreach}
</table>
<hr />
<h3>Site admins</h3>
Your name is not listed!
<table class="listing">
<tr valign='top' class="heading">
<td colspan='4' width='60px' align='center'>
&nbsp;
</td>
<td>User name</td>
<td>First name</td>
<td>Last name</td>
<td>Email</td>
<td>Rank</td>
</tr>
{foreach from=$admins item=admin name=forloop}
    {if $admin.rank < $user.rank}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td align='center'>
    {if ($admin.rank+1) < $user.rank}
    {submit_link action=increaseadminrank user_name=$admin.user_name src=$increase_img}
    {/if}
    </td>
    <td align='center'>
    {if $admin.rank > 1}
    {submit_link action=decreaseadminrank user_name=$admin.user_name src=$decrease_img}
    {/if}
    </td>
    <td align='center'>
    {submit_link action=deleteadmin user_name=$admin.user_name src=$del_img confirm_msg="Are you sure you want to delete this admin?"}
    </td>
    <td>
    {ups user=$admin}</td>
    <td>
    {$admin.user_name}
    </td>
    <td>{$admin.first_name}</td>
    <td>{$admin.last_name}</td>
    <td>{$admin.email}</td>
    <td>{$admin.rank}</td>
    </tr>
    {/if}
{/foreach}
</table>
{include file='footer.tpl'}
