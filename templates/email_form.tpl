{*
 * email_form.tpl
 *}
{include file='header.tpl'}
<form action='index.php' id="mail_form_id" method='post' onsubmit="return valid_mail_form();">
{csrf}
<input type="hidden" name="action" value="email_form_data" />
<input type="hidden" name="return_action" value="{$return_action}" />

{if isset($params)}
{foreach from=$params key=k item=v}
    <input type="hidden" name="return_{$k}" value="{$v}" />
{/foreach}
{/if}
<div align="center">
<h2>Send email</h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<table>
    {foreach from=$to item=to_address name=forloop}
	    <tr>
		    <td>{if $smarty.foreach.forloop.first}To{/if}</td>
		    <td><input type="text" name="to[]" id="to-{$smarty.foreach.forloop.index - 1}" value="{$to_address}" size="50" /></td>
	    </tr>
	{/foreach}
    {foreach from=$cc item=cc_address name=forloop}
	    <tr>
		    <td>{if $smarty.foreach.forloop.first}CC{/if}</td>
		    <td><input type="text" name="cc[]" id="cc-{$smarty.foreach.forloop.index - 1}" value="{$cc_address}"size="50" /></td>
	    </tr>
	{/foreach}
    {foreach from=$bcc item=bcc_address name=forloop}
	    <tr>
		    <td>{if $smarty.foreach.forloop.first}BCC{/if}</td>
		    <td><input type="text" name="bcc[]" id="bcc-{$smarty.foreach.forloop.index - 1}" value="{$bcc_address}"size="50" /></td>
	    </tr>
	{/foreach}
	<tr>
	    <td></td>
	    <td>
        <input type="submit" name="addTo" value="Add Recipient"/>
        <input type="submit" name="addCc" value="Add CC"/>
        <input type="submit" name="addBcc" value="Add BCC"/>
        </td>
	</tr>
	<tr>
	    <td>Subject</td>
	    <td><input type="text" name="subject" id="subject_id" value="{$subject}" size="50" /></td>
	</tr>
	<tr>
	    <td>Body</td>
	    <td><textarea name="body" id="body_id" cols="60" rows="15" class="textArea">{$body}</textarea></td>
	</tr>
	<tr>
		<td><input type="submit" name="emailsent" id="emailsent_id" value="Send" /></td>
		<td><input type="submit" name="emailcanceled" id="emailcanceled_id" value="Cancel" /></td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
