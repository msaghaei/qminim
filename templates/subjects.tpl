{*
 * subjects.tpl
 *}
{paging numrows=$numrows currentpage=$currentpage}
<table class='listing'>
<tr valign='top' class='heading'>
<td colspan='3' width='60px' align='center'>
{if $status != 'Finished' && count($subjects) <= $max_allowed_subjects}
{submit_link action=addsubject src=$add_img}
{if $testmode}
{submit_link action=enrollall src=$enroll_all_img}
{/if}
{else}
{$addd_img}
{/if}
{selmenu selected_treatments=$selected_treatments selected_factors=$selected_factors}
{$subjects_tt|tts:""}
</td>
<td width='50px'>{submit_link action=select_subject_id label=ID tag=a same_case=yes prompt_msg="selected_id:Enter subject ID (All to select all):$selected_id"}{if strtolower($selected_id) != 'all'} = {$selected_id}{/if}</td>
<td>Treatment</td>
{foreach from=$factors key=factor item=v}
    <td>{$factor}</td>
{/foreach}
</tr>
{foreach from=$subjects key=id item=subject name=forloop}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td align='center'>
    {submit_link action=editsubject id=$id src=$edit_img currentpage=$currentpage confirm_msg="Editing enrolled subjects may compromize the minimization.\\nAre you sure you want editing this subject?"}
    </td>
    <td align='center'>
    {submit_link action=deletesubject id=$id src=$del_img currentpage=$currentpage confirm_msg="Are you sure you want to delete this subject?"}
    </td>
    <td>
    {sps subject=$subject}
    </td>
    <td>{$id}</td>
    <td>{$subject.treatment}</td>
    {foreach from=$factors key=factor item=values}
        <td>{$subject.$factor}</td>
    {/foreach}
    </tr>
{/foreach}
</table>
{paging numrows=$numrows currentpage=$currentpage}
