{*
 * balance.tpl
 *}
<table class='listing' id='balance-table'>
<tr valign='top' class='heading'>
<td align='left'><div class="clicker"><img onclick="hideShow('{$base_url}');" id="expand_collapse_id" state="expand" src="{$base_url}templates/images/expand.gif" alt="Collapse" title="Collapse" /></div>
 {$balance_tt|tts:""}
</td>
<td align='center'>Marginal Balance</td>
<td align='center'>Range</td>
<td align='center'>Variance</td>
<td align='center'>SD</td>
</tr>
{foreach from=$balance key=i item=bl}
    <tr valign="top" class="{if ! $bl[0]}odd{else}even{/if}">
    {foreach from=$bl key=j item=item}
        {if $j < 2}
            {if $item and $j == 0}
                <th align='left'>{$item}</th>
            {else}
                {if $item}
                    <td align='right'>{$item}</td>
                {/if}
            {/if}
        {else}
            <td align='center'>{$item|string_format:"%.2f"}</td>
        {/if}
    {/foreach}
</tr>
{/foreach}
</table>

