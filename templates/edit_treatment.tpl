{*
 * edit_treatment.tpl
 *}
{include file='header.tpl'}
<div align="center">
<h2>Edit Treatment: <b>{$initialtreatment}</b></h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<form action='index.php' method='post' onsubmit="return valid_treatment_form({$max_allowed_chars});">
{csrf}
<input type="hidden" name="action" value="treatmentedited" />
<input type="hidden" name="initialtreatment" value="{$initialtreatment}" />
<table>
<tr>
<td>{$valididentifier_tt|tts|replace:'%s':$max_allowed_chars|replace:'%id_name':treatment}Treatment Name</td><td><input onChange="trim_value(this);" id="treatment_name_id" type='text' name='treatment' size='20' value='{$treatment}'></td>
</tr>
<tr>
<td>{$treatmentratio_tt|tts}Treatment Ratio</td><td><input onChange="trim_value(this);" id="treatment_ratio_id" type='text' name='ratio' size='6' value='{$ratio}'></td>
</tr>
<tr>
<td><input type="submit" name="submit" value="Save" class="defaultButton" /></td>
<td>{submit_link action=index_page label=Return subpage="treatments.tpl"}</td>
</tr>
</table>
</form>
</div>
{include file='footer.tpl'}
