{*
 * disabled.tpl
 *}
{include file='header.tpl'}
<h2>QMinim: Free online minimisation </h2>
<h3>Your account is disabled!</h3>
Reason: {$disabled_reason}<br />
Please contact site admin ({$admin_email})<br />
Click <b>{submit_link action=login_page label=here tag=a alt=login_page}</b> to login.
{include file='footer.tpl'}
