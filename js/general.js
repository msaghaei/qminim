if(!String.prototype.trim) {  
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g,'');  
  };  
}

if(!Array.prototype.trim) {  
    Array.prototype.trim = function () {
        for (i=0;i<this.length;i++){
            this[i] = this[i].trim();
        }
        return this;
    };
}

function levelCatClicked(max_levels) {
    var tb = document.getElementById('temp_level_id');
    var format = "Min Max Interval";
    sp = tb.value.split(" ");
    if (sp.length != 3) {
        alert("Enter three numbers for minimum, maximum and interval values as:\n" + format);
        return false;
    }
    for (i=0;i<sp.length;i++) {
        n = parseInt(sp[i]);
        if (! n) {
            alert("Enter three numbers for minimum, maximum and interval values as:\n" + format);
            return false
        }
    }
    minimum = parseInt(sp[0]);
    maximum = parseInt(sp[1]);
    interval = parseInt(sp[2]);
    interval_nums = parseInt((maximum - minimum) / interval);
    if (interval_nums > max_levels) {
        alert("(" + interval_nums + ") Number of intervals must not be more than " + max_levels);
        return false;
    }
    start = minimum;
    end = start + interval - 1;
    clearLevels();
    while (start < (maximum - 1)) {
        v = String(start)+"-"+String(end);
        addLevel(v, max_levels);
        start += interval;
        end += interval;
        if (end > maximum)
            end = maximum;
    }
}

function clearLevels() {
    document.getElementById('levels_id').innerHTML = "";
}

function validLevel(level, options) {
    if (! level) return false;
    for (i=0; i < options.length; i++) {
        option = options.item(i);
        if (option.text == level) return false;
    }
    return true;
}

function validIdentifier(name) {
    return /^[a-z][a-z0-9\-_:.]*$/i.test(name); 
}

function levelPressed(tb, e, max_levels) {
    if (e.keyCode == 13) {
        addLevel(tb.value, max_levels);
        tb.value='';
        return false;
    }
}

function levelAddClicked(max_levels) {
    var tb = document.getElementById('temp_level_id');
    addLevel(tb.value, max_levels);
    tb.value='';
}

function addLevel(v, max_levels) {
    var sel = document.getElementById('levels_id');
    if (sel.options.length < max_levels) {
        if (validLevel(v, sel.options)) {
            var opt = new Option(v, v);
            sel.options.add(opt);
        }
    }
}

function levelDeleteClicked() {
    var sel = document.getElementById('levels_id');
    idx = sel.selectedIndex;
    if (idx < 0) return;
    sel.remove(idx);
}

function setLevelBoxText() {
    var sel = document.getElementById('levels_id');
    var tb = document.getElementById('temp_level_id');
    var v = sel.options.item(sel.selectedIndex).text;
    tb.value = v;
}

function levelChangeClicked() {
    var tb = document.getElementById('temp_level_id');
    var sel = document.getElementById('levels_id');
    if (validLevel(tb.value, sel.options))
        sel.options.item(sel.selectedIndex).text = tb.value;
    tb.value='';
}

function set_body_width() {
    w = screen.width;
    sw = 0.95 * w;
    body = document.getElementsByTagName("body")[0];
    body.style.width = sw+"px";
}

function update_preload(obj) {
    var newvalue = parseInt(obj.value);
    var name = obj.name.split('_');

    var level_id = 'total_'+name[1]+'-'+name[2];
    var level = document.getElementById(level_id);

    var treatment_id = name[0]+'_total_id';
    var treatment = document.getElementById(treatment_id);

    var inputs = document.getElementsByTagName('input');
    
    var total_toal = document.getElementById('total_total');

    var this_factor = 0;
    var this_level = newvalue;
    var all_factors = new Object();
    all_factors[name[1]] = newvalue;
    var all_treatments = 0;
    for (i=0;i<inputs.length;i++) {
        if (inputs.item(i).type == 'text') {
            var item_name_arr = inputs.item(i).name.split('_');
            if ((item_name_arr.length == 2) && (item_name_arr[0] != name[0]) && (item_name_arr[1] == 'total')) {
                all_treatments += parseInt(inputs.item(i).value);
            }
            if (item_name_arr.length == 2) continue;
            if (name[0] != item_name_arr[0]) {
                if (name[1] == item_name_arr[1] && name[2] == item_name_arr[2]) {
                    this_level += parseInt(inputs.item(i).value);
                }
                continue;
            }
            if (name[1] == item_name_arr[1]) {
                this_factor += parseInt(inputs.item(i).value);
            }
            if (inputs.item(i).name == obj.name) continue;
            if (item_name_arr[1] in all_factors) {
                all_factors[item_name_arr[1]] += parseInt(inputs.item(i).value);
            } else {
                all_factors[item_name_arr[1]] = parseInt(inputs.item(i).value);
            }
        }
    }
    level.value = this_level;
    treatment.value = this_factor;
    total_toal.value = all_treatments + this_factor;
    var uniques = new Object();
    for (f in all_factors) {
        uniques[all_factors[f]] = all_factors[f];
    }
    var out = new Array();
    for (i in uniques)
        out.push(i);
    var submit_button = document.getElementById('submit_preload_id');
    if (out.length > 1) {
        treatment.style.color = '#ff0000';
        total_toal.style.color = '#ff0000';
        submit_button.disabled = true;
    } else {
        treatment.style.color = '#000000';
        total_toal.style.color = '#000000';
        submit_button.disabled = false;
    }
    window.focus();
}

function valid_support_form () {
    if (! element_valued('subject_text_id', 'Please enter a subject!')) return false;
    if (! element_valued('comment_id', 'Please enter a message!')) return false;
    if (! element_valued('captcha_code_id', 'Please enter a value for security code!')) return false;
    return true;
}

function valid_pre_register_form() {
    if (! element_valued('email_id', 'Please enter your email!')) return false;
    if (! element_valued('captcha_code_id', 'Please enter a value for security code!')) return false;
    if (! check_email_field()) return false;
    return true;
}

function valid_activation_code_form() {
    if (! element_valued('email_id', 'Please enter your email!')) return false;
    if (! element_valued('activation_code_id', 'Please enter your activation code!')) return false;
    if (! element_valued('captcha_code_id', 'Please enter a value for security code!')) return false;
    if (! check_email_field()) return false;
    return true;
}

function valid_register_form() {
    if (! element_valued('salutation_id', 'Please select a title!')) return false;
    if (! element_valued('job_title_id', 'Please select a job title!')) return false;
    if (! element_valued('first_name_id', 'Please enter a value for first name!')) return false;
    if (! element_valued('last_name_id', 'Please enter a value for last name!')) return false;
    if (! element_valued('city_id', 'Please enter a value for city!')) return false;
    if (! element_valued('country_id', 'Please select a country!')) return false;
    if (! element_valued('affiliation_id', 'Please enter your affiliation!')) return false;
    if (! element_valued('user_name_id', 'Please enter your username!')) return false;
    if (! element_valued('password_id', 'Please enter your password!')) return false;
    if (! element_valued('password_confirm_id', 'Please confirm your password!')) return false;
    if (! passwords_match('password_id', 'password_confirm_id', 'The passwords do not match!')) return false;
    var elem = document.getElementById('eulacheck_id');
    if (! elem.checked) {
        alert('You have to accept terms and conditions!');
        elem.focus();
        return false;
    }
    if (! element_valued('captcha_code_id', 'Please enter a value for security code!')) return false;
    return true;
}

function passwords_match(pw_id, cf_id, msg) {
    var pw = document.getElementById(pw_id);
    var cf = document.getElementById(cf_id);
    if (pw.value != cf.value) {
        alert(msg);
        pw.focus();
        return false;
    }
    return true;
}

function valid_email(email) {
    var emails = email.split(";");
    for (var i=0;i<emails.length;i++) {
        var e = emails[i];
        if (! /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e.trim())){
            return (false);
        }
    }
    return true;
}

function check_email_field() {
    var elem = document.getElementById('email_id');
    email = elem.value;
    if (valid_email(email)) {
        var emails = email.split(";");
        emails = emails.trim();
        elem.value = emails.join(";")
        return true;
    }
    alert("Invalid E-mail Address! Please re-enter.");
    return (false);
}

function element_valued(elem_id, msg) {
    var elem = document.getElementById(elem_id);
    if (! elem) return true;
    if (! elem.value) {
        alert(msg);
        elem.focus();
        return false;
    }
    return elem;
}

function activate_submit(check, submit_button) {
    var obj = document.getElementById(submit_button);
    if (check.checked)
        obj.disabled = false;
    else
        obj.disabled = true;
}

function showTable(table) {
    var obj = document.getElementById(table+'_img');
    toggleHidShow(obj, table);
}

function valid_preload_form(form) {
    var inputs = document.getElementsByTagName('input');
    for (i=0;i<inputs.length;i++) {
        if (inputs.item(i).type == 'text')
            if (inputs.item(i).value != '0')
                if (! parseInt(inputs.item(i).value)) {
                    var name = inputs.item(i).name.split('_').join(', ');
                    var value = inputs.item(i).value;
                    inputs.item(i).focus();
                    inputs.item(i).select();
                    alert('Invalid value ' + value + '; for ' + name + '. Only integer values acceptable!');
                    return false;
                }
    }
    return true;
}

function valid_settings_form(max_subjects) {
    var sample_size = document.getElementById('sample_size_id');
    if (! sample_size.value) {
        alert('Please enter a value for sample size!');
        sample_size.focus();
        return false;
    }
    if (! isInteger(sample_size.value)) {
        alert('Sample size must be integer number!');
        sample_size.focus();
        return false;
    }
    var ss = parseInt(sample_size.value);
    if (ss < 10) {
        alert('Sample size must be at least 10!');
        sample_size.focus();
        return false;
    } else {
        if (ss > max_subjects) {
            alert('Maximum value for sample size is '+max_subjects+'!');
            sample_size.focus();
            return false;
        }
    }
    var bp = document.getElementById('base_prob_id');
    if (! bp.value) {
        alert('Please enter a value for base probability!');
        bp.focus();
        return false;
    }
    if (! isInteger(bp.value) && isFloat(bp.value)) {
        alert('base probability must be number!');
        bp.focus();
        return false;
    }
    bp_value = parseFloat(bp.value);
    if (bp_value <= 0 || bp_value >= 1) {
        alert('Base probability must be greater than zero and less than one!');
        bp.focus();
        return false;
    }
    return true;
}

function valid_treatment_form(max_chars) {
    var treatment = document.getElementById('treatment_name_id');
    if (! treatment.value) {
        alert('Please enter a name for treatment!');
        treatment.focus();
        return false;
    }
    if (treatment.value.length > max_chars) {
        alert('Maximum length for a treatment name is '+ max_chars+'!');
        treatment.focus();
        return false;
    }
    if (! validIdentifier(treatment.value)) {
        alert('Invalid treatment name!');
        treatment.focus();
        return false;
    }
    var ratio = document.getElementById('treatment_ratio_id');
    if (! ratio.value) {
        alert('Please enter a ratio for '+treatment.value+'!');
        ratio.focus();
        return false;
    }
    if (! isInteger(ratio.value)) {
        alert('Treatment ratio must be an integer!');
        ratio.focus();
        return false;
    } else if (ratio.value <= 0) {
        alert('Treatment ratio must be greater than zero!');
        ratio.focus();
        return false;
    }
    return true;
}

function valid_factor_form(max_levels, max_chars) {
    var factor = document.getElementById('factor_name_id');
    var factor_name = factor.value;
    if (! factor_name) {
        alert('Please enter a name for factor!');
        factor.focus();
        return false;
    }
    if (factor_name.length > max_chars) {
        alert('Maximum length for a factor name is '+ max_chars+'!');
        factor.focus();
        return false;
    }
    invalid_names = ['treatment', 'treatments', 'subjects', 'factors', 'levels', 'subject_levels'];
    if (! validIdentifier(factor_name) || (invalid_names.indexOf(factor_name.toLowerCase()) > -1)) {
        alert("Invalid factor name '" + factor_name + "' !");
        factor.focus();
        return false;
    }
    var weight = document.getElementById('factor_weight_id');
    if (! weight.value) {
        alert('Please enter a weight for '+factor_name+'!');
        weight.focus();
        return false;
    }
    if (! parseFloat(weight.value)) {
        alert('Factor weight must be a decimal number!');
        weight.focus();
        return false;
    }
    var sel = document.getElementById('levels_id');
    if (sel.options.length < 2) {
        alert('Please enter at least 2 levels for '+factor_name+'!');
        return false;
    }
    var a = new Array();
    for (i=0;i<sel.options.length;i++) {
        a.push(sel.options.item(i).text);
    }
    var levels = document.getElementById('levels_str_id');
    levels.value = a.join();
    return true;
}

function hideShow(base_url) {
    var oTable = document.getElementById('balance-table');
    var img = document.getElementById('expand_collapse_id');
    var state = img.getAttribute('state');
    rows = oTable.rows;
    if (state == 'collapse') {
        var state = 'expand';
        var alt = 'Collapse';
    } else {
        var state = 'collapse';
        var alt = 'Expand';
    }
    img.setAttribute('state', state);
    img.src = base_url + "templates/images/" + state + '.gif';
    img.alt = alt;
    img.title = alt;
    for (r=0; r<rows.length;r++){
        row = rows[r];
        if (row.className == "odd") {
            row.className = "odd collapsible";
        } else if (row.className == "odd collapsible") {
            row.className = "odd";
        }
    }
}

function submit_form(action, obj, confirm_msg) {
    if (! confirm_msg)
        confirm_msg = false;
    if (! obj)
        obj = {};
    if (confirm_msg) {
        if (!confirm(confirm_msg))
            return false;
    }
    var form = document.getElementById('jsform_id');
    var hidden = document.getElementById('jsform_action_id');
    hidden.value = action;
    for (v in obj)
      {
        var node = hidden.cloneNode(true);
        if (v == 'prompt_msg') {
            var ar = obj[v].split(':');
            node.value = '';
            node.name = ar[0];
            node.value = prompt(ar[1], ar[2]);
            if (! node.value || node.value == null) {
                return false;
            }
        } else {
            node.name = v;
            node.value = obj[v];
        }
        form.appendChild(node);
      }
    var node = hidden.cloneNode(true);
    node.name = 'xpos';
    node.value = window.pageXOffset;
    form.appendChild(node);
    var node = hidden.cloneNode(true);
    node.name = 'ypos';
    node.value = window.pageYOffset;
    form.appendChild(node);
    if (action == 'download') {
        document.getElementById('download_div_id').style.display = "none";
        document.getElementById('return_div_id').style.display = "inline";
    }
    form.submit();
}

function randomPassword(len)
{
    chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    pass = "";
    for(x=0;x<len;x++)
        {
            i = Math.floor(Math.random() * 62);
            pass += chars.charAt(i);
        }
    return pass;
}

function insert_random_password(password_id, password_confirm_id, len, show_pw)
{
    pw_str = randomPassword(len);
    if (show_pw) {
        pw_str = window.prompt ("Copy to clipboard: Ctrl+C, Enter", pw_str);
    }
    if (pw_str) {
        var pw = document.getElementById(password_id);
        var cf = document.getElementById(password_confirm_id);
        pw.value = pw_str;
        cf.value = pw_str;
    }
}

function valid_profile_form() {
    if (! element_valued('first_name_id', 'Please enter a value for first name!')) return false;
    if (! element_valued('last_name_id', 'Please enter a value for last name!')) return false;
    if (! element_valued('country_id', 'Please select a country!')) return false;
    if (! element_valued('city_id', 'Please enter a value for city!')) return false;
    if (! element_valued('affiliation_id', 'Please enter your affiliation!')) return false;
    return true;
}

function valid_change_password_form() {
    if (! element_valued('current_password_id', 'Please enter your current password!')) return false;
    if (! element_valued('new_password_id', 'Please enter new password!')) return false;
    if (! element_valued('new_password_confirm_id', 'Please conform new password!')) return false;
    if (! passwords_match('new_password_id', 'new_password_confirm_id', 'The passwords do not match!')) return false;
    return true;
}

function scroll_to(hash) {
    location.hash = "#" + hash;
}

function valid_add_user_form(len) {
    if (! element_valued('salutation_id', 'Please enter a value for salutation!')) return false;
    if (! element_valued('job_title_id', 'Please enter a value for job title!')) return false;
    if (! element_valued('first_name_id', 'Please enter a value for first name!')) return false;
    if (! element_valued('last_name_id', 'Please enter a value for last name!')) return false;
    if (! element_valued('email_id', 'Please enter email!')) return false;
    if (! check_email_field()) return false;
    if (! element_valued('city_id', 'Please enter a value for city!')) return false;
    if (! element_valued('country_id', 'Please select a country!')) return false;
    if (! element_valued('affiliation_id', 'Please enter affiliation!')) return false;
    if (! element_valued('user_name_id', 'Please enter username!')) return false;
    var password = document.getElementById('password_id');
    if (password.value) {
        if (! passwords_match('password_id', 'password_confirm_id', 'The passwords do not match!')) return false;
    } else {
        password.value = randomPassword(len);
        var confirm = document.getElementById('password_confirm_id');
        vonfirm.value = password.value
    }
    return true;
}

function valid_edit_user_form() {
    if (! element_valued('salutation_id', 'Please enter a value for salutation!')) return false;
    if (! element_valued('job_title_id', 'Please enter a value for job title!')) return false;
    if (! element_valued('first_name_id', 'Please enter a value for first name!')) return false;
    if (! element_valued('last_name_id', 'Please enter a value for last name!')) return false;
    if (! element_valued('email_id', 'Please enter email!')) return false;
    if (! check_email_field()) return false;
    if (! element_valued('city_id', 'Please enter a value for city!')) return false;
    if (! element_valued('country_id', 'Please select a country!')) return false;
    if (! element_valued('affiliation_id', 'Please enter affiliation!')) return false;
    return true;
}

function valid_send_message_form() {
    if (! element_valued('message_subject_id', 'Please enter a value for subject!')) return false;
    if (! element_valued('message_body_id', 'Please enter message body!')) return false;
    return true;
}

function valid_login_form() {
    if (! element_valued('user_name_id', 'Username!')) return false;
    if (! element_valued('password_id', 'Password!')) return false;
    if (! element_valued('captcha_code_id', 'Security code!')) return false;
    return true;
}

function valid_login_problem_form() {
    if (! element_valued('user_name_or_email_id', 'Please enter your username or your email!')) return false;
    if (! element_valued('captcha_code_id', 'Please enter a value for security code!')) return false;
    return true;
}

function valid_reset_password_form() {
    if (! element_valued('new_password_id', 'Please enter your password!')) return false;
    if (! element_valued('new_password_confirm_id', 'Please confirm your password!')) return false;
    if (! passwords_match('new_password_id', 'new_password_confirm_id', 'The passwords do not match!')) return false;
    return true;
}

function trim_value(obj) {
    //javascript:while(''+this.value.charAt(0)==' ')this.value=this.value.substring(1,this.value.length);
    obj.value = obj.value.trim();
}

function isInteger(value) {
    if ((undefined === value) || (null === value)) {
        return false;
    }
    return value % 1 == 0;
}

function isFloat(f) {
    return typeof(f)==="number";
}

function valid_mail_form() {
    var emails = ['to[]', 'cc[]', 'bcc[]'];
    for (j=0;j<emails.length;j++) {
        c = document.getElementsByName(emails[j]);
        for (i=0;i<c.length;i++) {
            if (c[i].value) {
                if (! valid_email(c[i].value)) {
                    alert('Some emails are not valid!');
                    c[i].focus();
                    return false;
                }
            }
        }
    }
    if (! element_valued('subject_id', 'Enter the email subject!')) return false;
    if (! element_valued('body_id', 'Enter body of the email!')) return false;
    return true;
}

function focus_first_box() {
    var elems = document.forms[0].elements;
    tags = ['INPUT', 'TEXTAREA'];
    types = ['text', 'textarea', 'password'];
    for (i=0;i<elems.length;i++) {
        var e = elems[i];
        if (tags.indexOf(e.tagName) > -1 && types.indexOf(e.type) > -1) {
            if (e.readOnly) continue;
            e.focus();
            e.select();
            return true;
        }
    }
    return false;
}

function valid_install_form() {
    if (! element_valued('data_dir_id', 'Please enter data directory!')) return false;
    if (! element_valued('base_url_id', 'Please enter base directory!')) return false;
    if (! element_valued('cache_dir_id', 'Please enter cache directory!')) return false;    
    if (! element_valued('admin_email_id', 'Please enter admin email!')) return false;    
    if (! element_valued('admin_username_id', 'Please enter admin username!')) return false;    
    if (! element_valued('admin_password_id', 'Please enter admin password!')) return false;    
    if (! element_valued('admin_confirm_id', 'Please confirm admin password!')) return false;    
    if (! passwords_match('admin_password_id', 'admin_confirm_id', 'The passwords do not match!')) return false;
    return true;
}

function toggle_display(div) {
    if (document.getElementById(div).style.display) {
        document.getElementById(div).style.display = "";
    } else {
        document.getElementById(div).style.display = "block";
    }
    document.getElementById(div).style.zIndex = "1";
}

function submit_select_form(treatments_str, levels_str) {
    treatments = treatments_str.split(",");
    levels = levels_str.split(",");
    var form = document.getElementById('jsform_id');
    var hidden = document.getElementById('jsform_action_id');
    hidden.value = 'set_subjects_filter';
    for (i=0;i<treatments.length;i++) {
        var node = hidden.cloneNode(true);
        node.name = 'selected_treatments_'+treatments[i];
        id = node.name+'_id';
        if (document.getElementById(id).checked)
            node.value = "selected";
        else
            node.value = "notselected";
        form.appendChild(node);
    }
    for (i=0;i<levels.length;i++) {
        var node = hidden.cloneNode(true);
        node.name = 'selected_factor_'+levels[i];
        id = node.name+'_id';
        if (document.getElementById(id).checked)
            node.value = "selected";
        else
            node.value = "notselected";
        form.appendChild(node);
    }
    var node = hidden.cloneNode(true);
    node.name = 'subpage';
    node.value = 'subjects.tpl';
    form.appendChild(node);

    var node = hidden.cloneNode(true);
    node.name = 'xpos';
    node.value = window.pageXOffset;
    form.appendChild(node);
    var node = hidden.cloneNode(true);
    node.name = 'ypos';
    node.value = window.pageYOffset;
    form.appendChild(node);
    form.submit();
}
